import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

class PossibleGear
{
	public final Character character;

	public PossibleGear(Character character)
	{
		this.character = character;
	}

	public void add(Number number)
	{
	}

	public int getNumber()
	{
		return 0;
	}
}

class Gear
	extends PossibleGear
{
	public final List<Number> numbers;

	public Gear()
	{
		super('*');
		this.numbers = new ArrayList<>();
	}

	@Override
	public void add(Number number)
	{
		this.numbers.add(number);
	}

	@Override
	public int getNumber()
	{
		return this.numbers.size() == 2 ? this.numbers.get(0).value * this.numbers.get(1).value : 0;
	}
}

class Number
{
	public final int x;
	public final int y;
	public int size;
	public int value;

	public Number(int x, int y, PossibleGear[][] characters)
	{
		this.x = x;
		this.y = y;
		this.size = 0;
		this.value = 0;
		while (Opgave3.isDigit(characters[x + this.size][y]))
		{
			this.value *= 10;
			this.value += Opgave3.toDigit(characters[x + this.size][y]);
			characters[x + this.size][y] = null;
			this.size += 1;
		}
	}

	public boolean checkSymbol(PossibleGear[][] characters)
	{
		boolean result = false;
		for (int index = this.x - 1; index <= this.x + this.size; index++)
		{
			result |= characters[index][this.y - 1] != null;
			result |= characters[index][this.y + 1] != null;
		}
		result |= characters[this.x - 1][this.y] != null;
		result |= characters[this.x + this.size][this.y] != null;
		return result;
	}

	public void addToGears(PossibleGear[][] characters, int x, int y)
	{
		if (characters[x][y] != null)
		{
			characters[x][y].add(this);
		}
	}

	public void addToGears(PossibleGear[][] characters)
	{
		for (int index = this.x - 1; index <= this.x + this.size; index++)
		{
			addToGears(characters, index, this.y - 1);
			addToGears(characters, index, this.y + 1);
		}
		addToGears(characters, this.x - 1, this.y);
		addToGears(characters, this.x + this.size, this.y);
	}
}

public class Opgave3
{
	public static final Scanner SCANNER = new Scanner(System.in);

	public static boolean isDigit(PossibleGear character)
	{
		return character != null && 48 <= character.character && character.character < 58;
	}

	public static int toDigit(PossibleGear character)
	{
		return character.character - 48;
	}

	public static final List<Number> numbers(PossibleGear[][] characters, int sizeX, int sizeY)
	{
		List<Number> result = new ArrayList<>();
		for (int y = 1; y <= sizeY; y++)
		{
			for (int x = 1; x <= sizeX; x++)
			{
				if (isDigit(characters[x][y]))
				{
					result.add(new Number(x, y, characters));
				}
			}
		}
		return result;
	}

	public static long part1()
	{
		long result = 0;
		List<String> lines = new ArrayList<>();
		while (SCANNER.hasNextLine())
		{
			lines.add(SCANNER.nextLine());
		}
		int sizeX = lines.get(0).length();
		int sizeY = lines.size();
		PossibleGear[][] characters = new PossibleGear[sizeX + 2][sizeY + 2];
		for (int y = 1; y <= sizeY; y++)
		{
			for (int x = 1; x <= sizeX; x++)
			{
				char character = lines.get(y - 1).toCharArray()[x - 1];
				characters[x][y] = character == '.' ? null : character == '*' ? new Gear() : new PossibleGear(character);
			}
		}
		List<Number> numbers = numbers(characters, sizeX, sizeY);
		for (Number number: numbers)
		{
			if (number.checkSymbol(characters))
			{
				result += number.value;
			}
		}
		return result;
	}

	public static long part2()
	{
		long result = 0;
		List<String> lines = new ArrayList<>();
		while (SCANNER.hasNextLine())
		{
			lines.add(SCANNER.nextLine());
		}
		int sizeX = lines.get(0).length();
		int sizeY = lines.size();
		PossibleGear[][] characters = new PossibleGear[sizeX + 2][sizeY + 2];
		for (int y = 1; y <= sizeY; y++)
		{
			for (int x = 1; x <= sizeX; x++)
			{
				char character = lines.get(y - 1).toCharArray()[x - 1];
				characters[x][y] = character == '.' ? null : character == '*' ? new Gear() : new PossibleGear(character);
			}
		}
		List<Number> numbers = numbers(characters, sizeX, sizeY);
		for (Number number: numbers)
		{
			number.addToGears(characters);
		}
		for (int y = 1; y <= sizeY; y++)
		{
			for (int x = 1; x <= sizeX; x++)
			{
				if (characters[x][y] != null)
				{
					result += characters[x][y].getNumber();
				}
			}
		}
		return result;
	}

	public static void main(String[] args)
	{
		System.out.println(part2());
	}
}
