package adventofcode;

import java.io.InputStream;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

class Tile
{
	public final Vector2DInt[] directions;

	public Tile(char character)
	{
		switch (character)
		{
			case '.':
				this.directions = Vector2DInt.DIRECTIONS4;
				break;
			case '^':
				this.directions = new Vector2DInt[]{Vector2DInt.NORTH};
				break;
			case 'v':
				this.directions = new Vector2DInt[]{Vector2DInt.SOUTH};
				break;
			case '<':
				this.directions = new Vector2DInt[]{Vector2DInt.WEST};
				break;
			case '>':
				this.directions = new Vector2DInt[]{Vector2DInt.EAST};
				break;
			default:
				this.directions = new Vector2DInt[]{};
				break;
		}
	}
}

public class Opgave23
	extends Opgave
{
	class Vertex
		extends Vector2DInt
	{
		public Set<Edge> edges;

		public Vertex(Vector2DInt vector)
		{
			super(vector.x, vector.y);
			this.edges = new TreeSet<>();
		}

		public void initialize(Set<Vertex> vertices)
		{
			for (Vertex vertex: vertices)
			{
				if (vertex != this)
				{
					Set<Vector2DInt> visited = new TreeSet<>(vertices);
					visited.remove(this);
					visited.remove(vertex);
					SortedSet<Long> paths = new TreeSet<>();
					walks(visited, this, vertex, false, paths);
					if (!paths.isEmpty())
					{
						this.edges.add(new Edge(vertex, paths.first() - vertices.size() + 1));
					}
				}
			}
		}
	}

	class Edge
		implements Comparable<Edge>
	{
		public final Vertex to;
		public final long distance;

		public Edge(Vertex to, long distance)
		{
			this.to = to;
			this.distance = distance;
		}

		@Override
		public int compareTo(Edge that)
		{
			return this.to.compareTo(that.to);
		}
	}

	public final int sizeX;
	public final int sizeY;
	public final Grid<Tile> tiles;
	public final Vector2DInt start;
	public final Vector2DInt end;

	public Opgave23(InputStream in)
	{
		super(in);
		this.sizeX = this.lines.get(0).length();
		this.sizeY = this.lines.size();
		this.tiles = new Grid<>(this.sizeX, this.sizeY);
		for (Vector2DInt vector: Vector2DInt.vectors(this.sizeX, this.sizeY))
		{
			this.tiles.set(vector, new Tile(this.lines.get(vector.y).charAt(vector.x)));
		}
		this.start = new Vector2DInt(this.lines.get(0).indexOf('.'), 0);
		this.end = new Vector2DInt(this.lines.get(this.sizeY - 1).indexOf('.'), this.sizeY - 1);
	}

	public void walks(Set<Vector2DInt> visited, Vector2DInt current, Vector2DInt end, boolean directions, SortedSet<Long> result)
	{
		if (!visited.contains(current) && 0 < this.tiles.get(current).directions.length)
		{
			visited.add(current);
			if (current.equals(end))
			{
				result.add((long) visited.size());
			}
			else
			{
				for (Vector2DInt direction: directions ? this.tiles.get(current).directions : Vector2DInt.DIRECTIONS4)
				{
					Vector2DInt next = current.add(direction);
					if (next.insideBoundingBox(this.sizeX, this.sizeY))
					{
						walks(visited, next, end, directions, result);
					}
				}
			}
			visited.remove(current);
		}
	}

	public Set<Vertex> vertices()
	{
		Set<Vertex> result = new TreeSet<>();
		for (Vector2DInt vector: Vector2DInt.vectors(this.sizeX, this.sizeY))
		{
			if (0 < this.tiles.get(vector).directions.length)
			{
				int count = 0;
				for (Vector2DInt direction: Vector2DInt.DIRECTIONS4)
				{
					Vector2DInt next = vector.add(direction);
					if (next.insideBoundingBox(this.sizeX, this.sizeY) && 0 < this.tiles.get(next).directions.length)
					{
						count += 1;
					}
				}
				if (3 <= count)
				{
					result.add(new Vertex(vector));
				}
			}
		}
		return result;
	}

	public Long walks(Set<Vertex> visited, Vertex current, Vertex end, long distance)
	{
		Long result;
		if (!visited.contains(current))
		{
			visited.add(current);
			if (current.equals(end))
			{
				result = distance;
			}
			else
			{
				result = null;
				for (Edge edge: current.edges)
				{
					Long max = walks(visited, edge.to, end, distance + edge.distance);
					if (result == null || (max != null && result < max))
					{
						result = max;
					}
				}
			}
			visited.remove(current);
		}
		else
		{
			result = null;
		}
		return result;
	}

	public long part1()
	{
		SortedSet<Long> result = new TreeSet<>();
		walks(new TreeSet<>(), this.start, this.end, true, result);
		return result.last() - 1;
	}

	public long part2()
	{
		final Set<Vertex> vertices = vertices();
		Vertex start = new Vertex(this.start);
		Vertex end = new Vertex(this.end);
		vertices.add(start);
		vertices.add(end);
		vertices.forEach(vertex -> vertex.initialize(vertices));
		return walks(new TreeSet<>(), start, end, 0);
	}

	public static void main(String[] args)
	{
		Opgave23 opgave = new Opgave23(System.in);
		System.out.println(opgave.part1());
		System.out.println(opgave.part2());
	}
}