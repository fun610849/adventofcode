package adventofcode;

import java.io.InputStream;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class Opgave2
	extends Opgave
{
	public static final Scanner SCANNER = new Scanner(System.in);
	public static final String GREEN = "green";
	public static final String RED = "red";
	public static final String BLUE = "blue";

	public Opgave2(InputStream in)
	{
		super(in);
	}

	public static void update(String color, int count, Map<String, Integer> result)
	{
		int current = result.get(color);
		if (current < count)
		{
			result.put(color, count);
		}
	}

	public long part1()
	{
		long result = 0;
		for (String line: this.lines)
		{
			Map<String, Integer> maximum = new TreeMap<>();
			maximum.put(GREEN, 0);
			maximum.put(RED, 0);
			maximum.put(BLUE, 0);
			String[] game = line.split(": ");
			int id = Integer.parseInt(game[0].split(" ")[1]);
			String[] draws = game[1].split("; ");
			for (String draw: draws)
			{
				String[] countColors = draw.split(", ");
				for (String countColor: countColors)
				{
					String[] data = countColor.split(" ");
					int count = Integer.parseInt(data[0]);
					String color = data[1];
					update(color, count, maximum);
				}
			}
			if (maximum.get(RED) <= 12 && maximum.get(GREEN) <= 13 && maximum.get(BLUE) <= 14)
			{
				result += id;
			}
		}
		return result;
	}

	public long part2()
	{
		long result = 0;
		for (String line: this.lines)
		{
			Map<String, Integer> maximum = new TreeMap<>();
			maximum.put(GREEN, 0);
			maximum.put(RED, 0);
			maximum.put(BLUE, 0);
			String[] game = line.split(": ");
			int id = Integer.parseInt(game[0].split(" ")[1]);
			String[] draws = game[1].split("; ");
			for (String draw: draws)
			{
				String[] countColors = draw.split(", ");
				for (String countColor: countColors)
				{
					String[] data = countColor.split(" ");
					int count = Integer.parseInt(data[0]);
					String color = data[1];
					update(color, count, maximum);
				}
			}
			long power = (long) maximum.get(RED) * maximum.get(GREEN) * maximum.get(BLUE);
			result += power;
		}
		return result;
	}

	public static void main(String[] args)
	{
		Opgave2 opgave = new Opgave2(System.in);
		System.out.println(opgave.part1());
		System.out.println(opgave.part2());
	}
}