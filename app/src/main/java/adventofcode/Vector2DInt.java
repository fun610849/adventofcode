package adventofcode;

import java.util.ArrayList;
import java.util.List;

public class Vector2DInt
	implements Comparable<Vector2DInt>
{
	public static final Vector2DInt NORTH = new Vector2DInt(0, -1);
	public static final Vector2DInt SOUTH = new Vector2DInt(0, 1);
	public static final Vector2DInt WEST = new Vector2DInt(-1, 0);
	public static final Vector2DInt EAST = new Vector2DInt(1, 0);
	public static final Vector2DInt[] DIRECTIONS4 = new Vector2DInt[]{NORTH, SOUTH, WEST, EAST};

	public static List<Vector2DInt> vectors(int sizeX, int sizeY)
	{
		List<Vector2DInt> result = new ArrayList<>();
		for (int x = 0; x < sizeX; x++)
		{
			for (int y = 0; y < sizeY; y++)
			{
				result.add(new Vector2DInt(x, y));
			}
		}
		return result;
	}

	public final int x;
	public final int y;

	public Vector2DInt(int x, int y)
	{
		this.x = x;
		this.y = y;
	}

	@Override
	public int compareTo(Vector2DInt that)
	{
		int result = Long.compare(this.x, that.x);
		if (result == 0)
		{
			result = Long.compare(this.y, that.y);
		}
		return result;
	}

	public boolean equals(Vector2DInt that)
	{
		return compareTo(that) == 0;
	}

	public Vector2DInt add(Vector2DInt vector)
	{
		return new Vector2DInt(this.x + vector.x, this.y + vector.y);
	}

	public Vector2DInt subtract(Vector2DInt vector)
	{
		return add(vector.multiply(-1));
	}

	public boolean insideBoundingBox(long sizeX, long sizeY)
	{
		return 0 <= this.x && this.x < sizeX && 0 <= this.y && this.y < sizeY;
	}

	public Vector2DInt multiply(int factor)
	{
		return new Vector2DInt(factor * this.x, factor * this.y);
	}

	@Override
	public String toString()
	{
		return "(" + this.x + ", " + this.y + ")";
	}
}