package adventofcode;

import nl.novit.util.Function2;
import nl.novit.util.Tuple2Comparable;
import nl.novit.util.Wrapper;

import java.io.InputStream;
import java.util.Set;

class Position
	extends Tuple2Comparable<Integer, Integer>
{
	public static final Position ORIGIN = new Position(0, 0);

	public Position(Integer x, Integer y)
	{
		super(x, y);
	}

	public int getX()
	{
		return this.value0;
	}

	public int getY()
	{
		return this.value1;
	}
}

abstract class Direction
{
	public static final Direction[] DIRECTIONS = new Direction[]{new DirectionNorth(), new DirectionSouth(), new DirectionEast(), new DirectionWest()};

	public abstract int deltaX();

	public abstract int deltaY();

	public abstract boolean connects(Pipe pipe);

	public Position next(Position position)
	{
		return new Position(position.getX() + deltaX(), position.getY() + deltaY());
	}

	public Direction reverse()
	{
		Direction result = null;
		for (Direction direction: DIRECTIONS)
		{
			if (Position.ORIGIN.equals(direction.next(next(Position.ORIGIN))))
			{
				result = direction;
			}
		}
		return result;
	}

	public boolean equals(Direction that)
	{
		return this.deltaX() == that.deltaX() && this.deltaY() == that.deltaY();
	}
}

class DirectionNorth
	extends Direction
{
	@Override
	public int deltaX()
	{
		return 0;
	}

	@Override
	public int deltaY()
	{
		return -1;
	}

	@Override
	public boolean connects(Pipe pipe)
	{
		return pipe.north;
	}
}

class DirectionSouth
	extends Direction
{
	@Override
	public int deltaX()
	{
		return 0;
	}

	@Override
	public int deltaY()
	{
		return 1;
	}

	@Override
	public boolean connects(Pipe pipe)
	{
		return pipe.south;
	}
}

class DirectionEast
	extends Direction
{
	@Override
	public int deltaX()
	{
		return 1;
	}

	@Override
	public int deltaY()
	{
		return 0;
	}

	@Override
	public boolean connects(Pipe pipe)
	{
		return pipe.east;
	}
}

class DirectionWest
	extends Direction
{
	@Override
	public int deltaX()
	{
		return -1;
	}

	@Override
	public int deltaY()
	{
		return 0;
	}

	@Override
	public boolean connects(Pipe pipe)
	{
		return pipe.west;
	}
}

class Pipe
{
	public static final Pipe[] PIPES = new Pipe[]
		{
			new Pipe('F', false, true, true, false),
			new Pipe('7', false, true, false, true),
			new Pipe('J', true, false, false, true),
			new Pipe('L', true, false, true, false),
			new Pipe('-', false, false, true, true),
			new Pipe('|', true, true, false, false)
		};

	public static Pipe pipe(char character)
	{
		Pipe result = null;
		for (Pipe pipe: PIPES)
		{
			if (character == pipe.character)
			{
				result = pipe;
			}
		}
		return result;
	}

	public final char character;
	public final boolean north;
	public final boolean south;
	public final boolean east;
	public final boolean west;

	public Pipe(char character, boolean north, boolean south, boolean east, boolean west)
	{
		this.character = character;
		this.north = north;
		this.south = south;
		this.east = east;
		this.west = west;
	}

	@Override
	public String toString()
	{
		return String.valueOf(this.character);
	}
}

public class Opgave10
	extends Opgave
{
	public final int sizeX;
	public final int sizeY;
	public final Pipe[][] pipes;
	public Position animal;

	public Opgave10(InputStream in)
	{
		super(in);
		this.sizeX = this.lines.get(0).length();
		this.sizeY = this.lines.size();
		this.pipes = new Pipe[this.sizeX + 2][this.sizeY + 2];
		for (int x = 1; x <= this.sizeX; x++)
		{
			for (int y = 1; y <= this.sizeY; y++)
			{
				char character = this.lines.get(y - 1).charAt(x - 1);
				Pipe pipe = Pipe.pipe(character);
				this.pipes[x][y] = pipe;
			}
		}
		for (int x = 1; x <= this.sizeX; x++)
		{
			for (int y = 1; y <= this.sizeY; y++)
			{
				char character = this.lines.get(y - 1).charAt(x - 1);
				if (character == 'S')
				{
					Position position = new Position(x, y);
					this.animal = position;
					for (int index = 0; pipe(position) == null && index < Pipe.PIPES.length; index++)
					{
						Pipe pipe = Pipe.PIPES[index];
						this.pipes[x][y] = pipe;
						int count = 0;
						for (Direction direction: Direction.DIRECTIONS)
						{
							if (connects(position, direction))
							{
								count += 1;
							}
						}
						if (count != 2)
						{
							this.pipes[x][y] = null;
						}
					}
				}
			}
		}
	}

	public Pipe pipe(Position position)
	{
		return this.pipes[position.getX()][position.getY()];
	}

	public boolean connects(Position position, Direction direction)
	{
		Position positionNext = direction.next(position);
		Pipe pipe = pipe(position);
		Pipe pipeNext = pipe(positionNext);
		return pipe != null && pipeNext != null && direction.connects(pipe) && direction.reverse().connects(pipeNext);
	}

	public void walk(Position position, Direction direction, Function2<Boolean, Position, Direction> function)
	{
		if (connects(position, direction) && function.apply(direction.next(position), direction))
		{
			for (Direction directionNext: Direction.DIRECTIONS)
			{
				if (!directionNext.equals(direction.reverse()))
				{
					walk(direction.next(position), directionNext, function);
				}
			}
		}
	}

	public void walk(Position position, Function2<Boolean, Position, Direction> function)
	{
		for (Direction direction: Direction.DIRECTIONS)
		{
			walk(position, direction, function);
		}
	}

	public long part1()
	{
		final Wrapper<Integer> distance = new Wrapper<>(0);
		Function2<Boolean, Position, Direction> function = (position, direction) ->
		{
			boolean result = distance.value == 0 || !position.equals(Opgave10.this.animal);
			distance.value += 1;
			return result;
		};
		walk(this.animal, function);
		return distance.value / 4;
	}

	public boolean inside(boolean[][] loop, Position position)
	{
		boolean result = !loop[position.getX()][position.getY()];
		boolean inside = false;
		while (position.getX() <= this.sizeX && position.getY() <= this.sizeY)
		{
			if (loop[position.getX()][position.getY()] && Set.of('F', 'J', '|', '-').contains(this.pipes[position.getX()][position.getY()].character))
			{
				inside = !inside;
			}
			position = new Position(position.getX() + 1, position.getY() + 1);
		}
		return result && inside;
	}

	public long part2()
	{
		final boolean[][] loop = new boolean[this.sizeX + 2][this.sizeY + 2];
		Function2<Boolean, Position, Direction> functionLoop = (position, direction) ->
		{
			loop[position.getX()][position.getY()] = true;
			return !position.equals(Opgave10.this.animal);
		};
		walk(this.animal, functionLoop);
		int result = 0;
		for (int x = 1; x <= this.sizeX; x++)
		{
			for (int y = 1; y <= this.sizeY; y++)
			{
				if (inside(loop, new Position(x, y)))
				{
					result += 1;
				}
			}
		}
		return result;
	}

	public static void main(String[] args)
	{
		Opgave10 opgave = new Opgave10(System.in);
		System.out.println(opgave.part1());
		System.out.println(opgave.part2());
	}
}
