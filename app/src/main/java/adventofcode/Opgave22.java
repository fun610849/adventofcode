package adventofcode;

import nl.novit.util.Util;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

class Brick
	implements Comparable<Brick>
{
	public static final Vector3DInt DOWN = new Vector3DInt(0, 0, -1);
	public final Vector3DInt start;
	public final Vector3DInt end;

	public Brick(Vector3DInt start, Vector3DInt end)
	{
		this.start = start;
		this.end = end;
	}

	public Brick(String input)
	{
		this
			(
				new Vector3DInt(input.split("~")[0]),
				new Vector3DInt(input.split("~")[1])
			);
	}

	@Override
	public int compareTo(Brick that)
	{
		int result = this.start.compareTo(that.start);
		if (result == 0)
		{
			result = this.end.compareTo(that.end);
		}
		return result;
	}

	public Vector3DInt direction()
	{
		int x = -Integer.compare(this.start.x, this.end.x);
		int y = -Integer.compare(this.start.y, this.end.y);
		int z = -Integer.compare(this.start.z, this.end.z);
		return new Vector3DInt(x, y, z);
	}

	public boolean contains(Vector3DInt vector)
	{
		boolean result = this.end.equals(vector);
		Vector3DInt loop = this.start;
		while (!loop.equals(this.end))
		{
			result |= loop.equals(vector);
			loop = loop.add(direction());
		}
		return result;
	}

	public boolean isSupported(List<Brick> bricks, Vector3DInt vector)
	{
		boolean result = vector.z == 1;
		vector = vector.add(DOWN);
		for (Brick brick: bricks)
		{
			result |= brick != this && brick.contains(vector);
		}
		return result;
	}

	public boolean isSupported(List<Brick> bricks)
	{
		boolean result = isSupported(bricks, this.end);
		Vector3DInt loop = this.start;
		while (!loop.equals(this.end))
		{
			result |= isSupported(bricks, loop);
			loop = loop.add(direction());
		}
		return result;
	}
}

public class Opgave22
	extends Opgave
{
	public static List<Brick> fallOnce(final List<Brick> bricks)
	{
		return bricks
			.stream()
			.parallel()
			.map(brick -> brick.isSupported(bricks) ? brick : new Brick(brick.start.add(Brick.DOWN), brick.end.add(Brick.DOWN)))
			.collect(Collectors.toList());
	}

	public static List<Brick> fall(List<Brick> bricks)
	{
		List<Brick> result = fallOnce(bricks);
		while (Util.compareIterable(result, bricks) != 0)
		{
			bricks = result;
			result = fallOnce(bricks);
		}
		return result;
	}

	public static long falling(List<Brick> bricks)
	{
		long result = 0;
		List<Brick> fallen = fall(bricks);
		for (int index = 0; index < bricks.size(); index++)
		{
			if (bricks.get(index).compareTo(fallen.get(index)) != 0)
			{
				result += 1;
			}
		}
		return result;
	}

	public final List<Brick> bricks;

	public Opgave22(InputStream in)
	{
		super(in);
		List<Brick> bricks = new ArrayList<>();
		for (String line: this.lines)
		{
			bricks.add(new Brick(line));
		}
		this.bricks = fall(bricks);
	}

	public long part1()
	{
		long result = 0;
		for (Brick brick: this.bricks)
		{
			List<Brick> bricks = new ArrayList<>(this.bricks);
			bricks.remove(brick);
			if (falling(bricks) == 0)
			{
				result += 1;
			}
		}
		return result;
	}

	public long part2()
	{
		long result = 0;
		for (Brick brick: this.bricks)
		{
			List<Brick> bricks = new ArrayList<>(this.bricks);
			bricks.remove(brick);
			result += falling(bricks);
		}
		return result;
	}

	public static void main(String[] args)
	{
		Opgave22 opgave = new Opgave22(System.in);
		System.out.println(opgave.part1());
		System.out.println(opgave.part2());
	}
}