package adventofcode;

public class Vector2DFraction
	implements Comparable<Vector2DFraction>
{
	public final Fraction x;
	public final Fraction y;

	public Vector2DFraction(Fraction x, Fraction y)
	{
		this.x = x;
		this.y = y;
	}

	public Vector2DFraction(long x, long y)
	{
		this(new Fraction(x), new Fraction(y));
	}

	public Vector2DFraction(Vector3DInt vector)
	{
		this(vector.x, vector.y);
	}

	public Vector2DFraction(String[] input)
	{
		this(Long.parseLong(input[0]), Long.parseLong(input[1]));
	}

	public Vector2DFraction(String input)
	{
		this(input.split(","));
	}

	@Override
	public int compareTo(Vector2DFraction that)
	{
		int result = this.x.compareTo(that.x);
		if (result == 0)
		{
			result = this.y.compareTo(that.y);
		}
		return result;
	}

	public boolean equals(Vector2DFraction that)
	{
		return compareTo(that) == 0;
	}

	public Vector2DFraction add(Vector2DFraction vector)
	{
		return new Vector2DFraction(this.x.add(vector.x), this.y.add(vector.y));
	}

	public boolean insideBoundingBox(Fraction sizeX, Fraction sizeY)
	{
		return
			Fraction.ZERO.compareTo(this.x) <= 0 &&
				this.x.compareTo(sizeX) <= 0 &&
				Fraction.ZERO.compareTo(this.y) <= 0 &&
				this.y.compareTo(sizeY) <= 0;
	}

	public Vector2DFraction multiply(Fraction factor)
	{
		return new Vector2DFraction(this.x.multiply(factor), this.y.multiply(factor));
	}

	@Override
	public String toString()
	{
		return "(" + this.x + ", " + this.y + ")";
	}
}