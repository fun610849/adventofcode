package adventofcode;

import java.io.InputStream;
import java.util.*;

class Hand
	implements Comparable<Hand>
{
	public static final int HIGH_CARD = 1;
	public static final int ONE_PAIR = 2;
	public static final int TWO_PAIR = 3;
	public static final int THREE_OF_A_KIND = 4;
	public static final int FULL_HOUSE = 5;
	public static final int FOUR_OF_A_KIND = 6;
	public static final int FIVE_OF_A_KIND = 7;
	public List<Integer> cards;
	public final int bet;
	public final int rank;

	public SortedMap<Integer, Integer> card2Count()
	{
		SortedMap<Integer, Integer> result = new TreeMap<>();
		this.cards.forEach
			(
				card ->
				{
					if (result.containsKey(card))
					{
						result.put(card, result.get(card) + 1);
					}
					else
					{
						result.put(card, 1);
					}
				}
			);
		return result;
	}

	public int highCount(SortedMap<Integer, Integer> cards2Count)
	{
		return cards2Count.values().stream().max(Comparator.naturalOrder()).get();
	}

	public boolean fiveOfAKind()
	{
		return card2Count().size() == 1;
	}

	public boolean fourOfAKind()
	{
		return highCount(card2Count()) == 4;
	}

	public boolean fullHouse()
	{
		SortedMap<Integer, Integer> card2Count = card2Count();
		return card2Count.size() == 2 && highCount(card2Count()) == 3;
	}

	public boolean threeOfAKind()
	{
		SortedMap<Integer, Integer> card2Count = card2Count();
		return card2Count.size() == 3 && highCount(card2Count) == 3;
	}

	public boolean twoPair()
	{
		SortedMap<Integer, Integer> card2Count = card2Count();
		return card2Count.size() == 3 && highCount(card2Count) == 2;
	}

	public boolean onePair()
	{
		SortedMap<Integer, Integer> card2Count = card2Count();
		return card2Count.size() == 4;
	}

	public boolean highCard()
	{
		return card2Count().size() == 5;
	}

	public int rank()
	{
		int result = 0;
		if (highCard())
		{
			result = HIGH_CARD;
		}
		if (onePair())
		{
			result = ONE_PAIR;
		}
		if (twoPair())
		{
			result = TWO_PAIR;
		}
		if (threeOfAKind())
		{
			result = THREE_OF_A_KIND;
		}
		if (fullHouse())
		{
			result = FULL_HOUSE;
		}
		if (fourOfAKind())
		{
			result = FOUR_OF_A_KIND;
		}
		if (fiveOfAKind())
		{
			result = FIVE_OF_A_KIND;
		}
		return result;
	}

	public int rankJoker()
	{
		int result = rank();
		for (int card0 = 0; card0 < Opgave7.CARDS.size(); card0++)
		{
			for (int card1 = 0; card1 < Opgave7.CARDS.size(); card1++)
			{
				for (int card2 = 0; card2 < Opgave7.CARDS.size(); card2++)
				{
					for (int card3 = 0; card3 < Opgave7.CARDS.size(); card3++)
					{
						for (int card4 = 0; card4 < Opgave7.CARDS.size(); card4++)
						{
							List<Integer> cards = new ArrayList<>();
							cards.add(this.cards.get(0) == 0 ? card0 : this.cards.get(0));
							cards.add(this.cards.get(1) == 0 ? card1 : this.cards.get(1));
							cards.add(this.cards.get(2) == 0 ? card2 : this.cards.get(2));
							cards.add(this.cards.get(3) == 0 ? card3 : this.cards.get(3));
							cards.add(this.cards.get(4) == 0 ? card4 : this.cards.get(4));
							List<Integer> original = this.cards;
							this.cards = cards;
							result = Math.max(result, rank());
							this.cards = original;
						}
					}
				}
			}
		}
		return result;
	}

	public Hand(String input)
	{
		String c = input.split("\\s+")[0];
		String b = input.split("\\s+")[1];
		this.cards = new ArrayList<>();
		for (int index = 0; index < 5; index++)
		{
			this.cards.add(Opgave7.CARDS.get(c.toCharArray()[index]));
		}
		this.bet = Integer.parseInt(b);
		this.rank = rankJoker();
	}

	@Override
	public int compareTo(Hand that)
	{
		int result = Integer.compare(this.rank, that.rank);
		for (int index = 0; result == 0 && index < 5; index++)
		{
			result = Integer.compare(this.cards.get(index), that.cards.get(index));
		}
		return result;
	}
}

public class Opgave7
	extends Opgave
{
	public static final Map<Character, Integer> CARDS = generateCards();

	public static Map<Character, Integer> generateCards()
	{
		Map<Character, Integer> result = new TreeMap<>();
		result.put('J', 0);
		result.put('2', 1);
		result.put('3', 2);
		result.put('4', 3);
		result.put('5', 4);
		result.put('6', 5);
		result.put('7', 6);
		result.put('8', 7);
		result.put('9', 8);
		result.put('T', 9);
		result.put('Q', 10);
		result.put('K', 11);
		result.put('A', 12);
		return result;
	}

	public Opgave7(InputStream in)
	{
		super(in);
	}

	public long part1()
	{
		long result = 0;
		List<Hand> hands = new ArrayList<>();
		lines.forEach(line -> hands.add(new Hand(line)));
		hands.sort(Comparator.naturalOrder());
		for (int index = 0; index < hands.size(); index++)
		{
			result += (long) (index + 1) * hands.get(index).bet;
		}
		return result;
	}

	public static void main(String[] args)
	{
		System.out.println(new Opgave7(System.in).part1());
	}
}