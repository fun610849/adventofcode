package adventofcode;

import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class Opgave4
	extends Opgave
{
	public Opgave4(InputStream in)
	{
		super(in);
	}

	public static Set<Integer> numbers(String numbers)
	{
		Set<Integer> result = new TreeSet<>();
		String[] split = numbers.split("\\s+");
		for (String part: split)
		{
			result.add(Integer.parseInt(part));
		}
		return result;
	}

	public static int[] count(List<String> lines)
	{
		int[] result = new int[lines.size()];
		for (int index = 0; index < lines.size(); index++)
		{
			String line = lines.get(index);
			String numbers = line.split(":\\s*")[1];
			Set<Integer> winnings = numbers(numbers.split("\\s*\\|\\s*")[0]);
			Set<Integer> havings = numbers(numbers.split("\\s*\\|\\s*")[1]);
			int count = 0;
			for (Integer having: havings)
			{
				if (winnings.contains(having))
				{
					count++;
				}
			}
			result[index] = count;
		}
		return result;
	}

	public long part1()
	{
		int[] score = new int[]{0, 1, 2, 4, 8, 16, 32, 64, 128, 256, 512};
		return Arrays.stream(count(this.lines)).map(count -> score[count]).sum();
	}

	public long part2()
	{
		int[] count = count(this.lines);
		long[] cards = new long[count.length];
		Arrays.fill(cards, 1);
		for (int indexCurrent = 0; indexCurrent < count.length; indexCurrent++)
		{
			for (int indexCopy = 0; indexCopy < count[indexCurrent]; indexCopy++)
			{
				cards[indexCurrent + indexCopy + 1] += cards[indexCurrent];
			}
		}
		return Arrays.stream(cards).sum();
	}

	public static void main(String[] args)
	{
		Opgave4 opgave = new Opgave4(System.in);
		System.out.println(opgave.part1());
		System.out.println(opgave.part2());
	}
}