package adventofcode;

import java.io.InputStream;

class Minimum
{
	public final Long[][] minimum;

	public Minimum(final int stepsMaximum)
	{
		this.minimum = new Long[Vector2DInt.DIRECTIONS4.length][stepsMaximum + 1];
	}

	public Long get(int indexDirectionNext, int stepsLeft)
	{
		return this.minimum[indexDirectionNext][stepsLeft];
	}

	public boolean set(int indexDirectionNext, int stepsLeft, long minimum)
	{
		boolean result = false;
		if (this.minimum[indexDirectionNext][stepsLeft] == null || minimum < this.minimum[indexDirectionNext][stepsLeft])
		{
			this.minimum[indexDirectionNext][stepsLeft] = minimum;
			result = true;
		}
		return result;
	}

	public boolean set(int indexDirectionNext, long minimum)
	{
		boolean result = false;
		for (int indexDirection = 0; indexDirection < Vector2DInt.DIRECTIONS4.length; indexDirection++)
		{
			if
			(
				indexDirection != indexDirectionNext
					&& !Vector2DInt.DIRECTIONS4[indexDirection].equals(Vector2DInt.DIRECTIONS4[indexDirectionNext].multiply(-1))
			)
			{
				for (int stepsLeft = 0; stepsLeft < this.minimum[indexDirection].length; stepsLeft++)
				{
					result |= set(indexDirection, stepsLeft, minimum);
				}
			}
		}
		return result;
	}
}

public class Opgave17
	extends Opgave
{
	public final int sizeX;
	public final int sizeY;
	public final Grid<Integer> losses;

	public Opgave17(InputStream in)
	{
		super(in);
		this.sizeX = this.lines.get(0).length();
		this.sizeY = this.lines.size();
		this.losses = new Grid<>(this.sizeX, this.sizeY);
		for (Vector2DInt vector: Vector2DInt.vectors(this.sizeX, this.sizeY))
		{
			this.losses.set(vector, this.lines.get(vector.y).charAt(vector.x) - 48);
		}
	}

	public Grid<Minimum> minimum(final int stepsMinimum, final int stepsMaximum)
	{
		final Grid<Minimum> result = new Grid<>(this.sizeX, this.sizeY, () -> new Minimum(stepsMaximum));
		result.get(new Vector2DInt(0, 0)).set(0, 0);
		result.get(new Vector2DInt(0, 0)).set(2, 0);
		boolean changed = true;
		while (changed)
		{
			changed = false;
			for (Vector2DInt position: Vector2DInt.vectors(this.sizeX, this.sizeY))
			{
				for (int indexDirectionNext = 0; indexDirectionNext < Vector2DInt.DIRECTIONS4.length; indexDirectionNext++)
				{
					Vector2DInt directionNext = Vector2DInt.DIRECTIONS4[indexDirectionNext];
					for (int steps = stepsMinimum; steps <= stepsMaximum; steps++)
					{
						Vector2DInt positionNext = position.add(directionNext.multiply(steps));
						if (positionNext.insideBoundingBox(this.sizeX, this.sizeY))
						{
							for (int stepsLeft = steps; stepsLeft <= stepsMaximum; stepsLeft++)
							{
								Long minimum = result.get(position).get(indexDirectionNext, stepsLeft);
								if (minimum != null)
								{
									for (int step = 1; step <= steps; step++)
									{
										minimum += this.losses.get(position.add(directionNext.multiply(step)));
									}
									changed |= result.get(positionNext).set(indexDirectionNext, stepsLeft - steps, minimum);
									changed |= result.get(positionNext).set(indexDirectionNext, minimum);
								}
							}
						}
					}
				}
			}
		}
		return result;
	}

	public long minimum(Grid<Minimum> minimum)
	{
		Long result = null;
		for (int indexDirection = 0; indexDirection < Vector2DInt.DIRECTIONS4.length; indexDirection++)
		{
			if (result == null || minimum.get(new Vector2DInt(this.sizeX - 1, this.sizeY - 1)).get(indexDirection, 0) < result)
			{
				result = minimum.get(new Vector2DInt(this.sizeX - 1, this.sizeY - 1)).get(indexDirection, 0);
			}
		}
		return result;
	}

	public long part1()
	{
		return minimum(minimum(1, 3));
	}

	public long part2()
	{
		return minimum(minimum(4, 10));
	}

	public static void main(String[] args)
	{
		Opgave17 opgave = new Opgave17(System.in);
		System.out.println(opgave.part1());
		System.out.println(opgave.part2());
	}
}