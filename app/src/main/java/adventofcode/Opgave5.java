package adventofcode;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

class Range
{
	public final long start_destination;
	public final long start_source;
	public final long size;

	public Range(String line)
	{
		String[] split = line.split("\\s+");
		this.start_destination = Long.parseLong(split[0]);
		this.start_source = Long.parseLong(split[1]);
		this.size = Long.parseLong(split[2]);
	}

	public Long get(long key)
	{
		return (this.start_source <= key && key < this.start_source + this.size) ? this.start_destination + key - this.start_source : null;
	}
}

class RangeMap
{
	public final List<Range> ranges;

	public RangeMap(List<String> lines)
	{
		this.ranges = new ArrayList<>();
		lines.remove(0);
		while (!lines.isEmpty() && !lines.get(0).isEmpty())
		{
			this.ranges.add(new Range(lines.get(0)));
			lines.remove(0);
		}
		if (!lines.isEmpty())
		{
			lines.remove(0);
		}
	}

	public long get(long key)
	{
		Long result = null;
		for (Range range: this.ranges)
		{
			if (result == null)
			{
				result = range.get(key);
			}
		}
		if (result == null)
		{
			result = key;
		}
		return result;
	}
}

public class Opgave5
	extends Opgave
{
	public static long seedToLocation(RangeMap[] maps, long result)
	{
		for (RangeMap map: maps)
		{
			result = map.get(result);
		}
		return result;
	}

	public Opgave5(InputStream in)
	{
		super(in);
	}

	public long part1()
	{
		final List<Long> seeds = Arrays.stream(lines.get(0).split(": ")[1].split("\\s+"))
			.map(Long::parseLong)
			.collect(Collectors.toList());
		lines.remove(0);
		lines.remove(0);
		final RangeMap[] maps = new RangeMap[]
			{
				new RangeMap(lines),
				new RangeMap(lines),
				new RangeMap(lines),
				new RangeMap(lines),
				new RangeMap(lines),
				new RangeMap(lines),
				new RangeMap(lines)
			};
		return seeds.stream()
			.map(seed -> seedToLocation(maps, seed))
			.min(Comparator.naturalOrder()).get();
	}

	public long part2()
	{
		final List<Long> seeds = Arrays.stream(lines.get(0).split(": ")[1].split("\\s+"))
			.map(Long::parseLong)
			.collect(Collectors.toList());
		lines.remove(0);
		lines.remove(0);
		final RangeMap[] maps = new RangeMap[]
			{
				new RangeMap(lines),
				new RangeMap(lines),
				new RangeMap(lines),
				new RangeMap(lines),
				new RangeMap(lines),
				new RangeMap(lines),
				new RangeMap(lines)
			};
		Long result = null;
		for (int index = 0; index < seeds.size(); index += 2)
		{
			long min = LongStream.range(seeds.get(index), seeds.get(index) + seeds.get(index + 1))
				.parallel()
				.map(seed -> seedToLocation(maps, seed))
				.min()
				.getAsLong();
			if (result == null || min < result)
			{
				result = min;
			}
		}
		return result;
	}

	public static void main(String[] args)
	{
		System.out.println(new Opgave5(System.in).part2());
	}
}