package adventofcode;

import nl.novit.util.Tuple2;
import nl.novit.util.Util;

import java.io.InputStream;
import java.util.*;

class Component
	implements Comparable<Component>
{
	public final String name;
	public final int index;
	public final Set<Component> connecteds;

	public Component(String name, int index)
	{
		this.name = name;
		this.index = index;
		this.connecteds = new TreeSet<>();
	}

	@Override
	public int compareTo(Component that)
	{
		return Integer.compare(this.index, that.index);
	}

	@Override
	public String toString()
	{
		return "(" + this.name + ": " + Util.toString(this.connecteds, " ", component -> component.name) + ")";
	}
}

public class Opgave25
	extends Opgave
{
	public SortedMap<String, Component> components;

	public Component add(final String component)
	{
		return this.components.computeIfAbsent(component, k -> new Component(component, this.components.size()));
	}

	public Opgave25(InputStream in)
	{
		super(in);
		this.components = new TreeMap<>();
		for (String line: this.lines)
		{
			String[] split = line.split(": ");
			final Component componentA = add(split[0]);
			Arrays.stream(split[1].split(" "))
				.map(this::add)
				.forEach
					(
						componentB ->
						{
							connect(new Tuple2<>(componentA, componentB));
						}
					);
		}
	}

	public boolean isConnected(Tuple2<Component, Component> connection)
	{
		return connection.value0.connecteds.contains(connection.value1);
	}

	public void connect(Tuple2<Component, Component> connection)
	{
		connection.value0.connecteds.add(connection.value1);
		connection.value1.connecteds.add(connection.value0);
	}

	public void connect(List<Tuple2<Component, Component>> connections)
	{
		connections.forEach(this::connect);
	}

	public void disconnect(Tuple2<Component, Component> connection)
	{
		connection.value0.connecteds.remove(connection.value1);
		connection.value1.connecteds.remove(connection.value0);
	}

	public void disconnect(List<Tuple2<Component, Component>> connections)
	{
		connections.forEach(this::disconnect);
	}

	public List<Tuple2<Component, Component>> path(Component componentStart, Component componentEnd)
	{
		Map<Component, Component> reverse = new TreeMap<>();
		Set<Component> froms = new TreeSet<>();
		froms.add(componentStart);
		while (!froms.isEmpty())
		{
			Set<Component> next = new TreeSet<>();
			for (Component from: froms)
			{
				for (Component connected: from.connecteds)
				{
					if (connected != componentStart && !reverse.containsKey(connected))
					{
						reverse.put(connected, from);
						next.add(connected);
					}
				}
			}
			froms = next;
		}
		List<Tuple2<Component, Component>> result;
		Component to = reverse.get(componentEnd);
		if (to != null)
		{
			result = new ArrayList<>();
			while (to != null)
			{
				result.add(new Tuple2<>(componentEnd, to));
				componentEnd = to;
				to = reverse.get(to);
			}
		}
		else
		{
			result = null;
		}
		return result;
	}

	public boolean connected(Component componentA, Component componentB, int paths)
	{
		boolean result;
		if (paths == 0)
		{
			result = true;
		}
		else
		{
			List<Tuple2<Component, Component>> path = path(componentA, componentB);
			if (path == null)
			{
				result = false;
			}
			else
			{
				disconnect(path);
				result = connected(componentA, componentB, paths - 1);
				connect(path);
			}
		}
		return result;
	}

	public long part1()
	{
		Set<Component> groupA = new TreeSet<>();
		Set<Component> groupB = new TreeSet<>();
		Component componentA = this.components.values().stream().findFirst().get();
		groupA.add(componentA);
		this.components.values()
			.stream()
			.skip(1)
			.forEach(component -> (connected(componentA, component, 4) ? groupA : groupB).add(component));
		return (long) groupA.size() * groupB.size();
	}

	public long part2()
	{
		long result = 0;
		return result;
	}

	public static void main(String[] args)
	{
		Opgave25 opgave = new Opgave25(System.in);
		System.out.println(opgave.part1());
		System.out.println(opgave.part2());
	}
}