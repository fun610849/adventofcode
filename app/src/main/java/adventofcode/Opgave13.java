package adventofcode;

import nl.novit.parser.exception.ExceptionParserInput;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class Opgave13
	extends Opgave
{
	public final List<List<String>> patterns;

	public Opgave13(InputStream in)
		throws ExceptionParserInput
	{
		super(in);
		this.patterns = new ArrayList<>();
		List<String> pattern = new ArrayList<>();
		for (String line: this.lines)
		{
			if (line.isEmpty())
			{
				this.patterns.add(pattern);
				pattern = new ArrayList<>();
			}
			else
			{
				pattern.add(line);
			}
		}
		this.patterns.add(pattern);
	}

	public static List<String> transpose(List<String> pattern)
	{
		List<String> result = new ArrayList<>();
		for (int x = 0; x < sizeX(pattern); x++)
		{
			String line = "";
			for (int y = 0; y < sizeY(pattern); y++)
			{
				line += rock(pattern, x, y) ? "#" : ".";
			}
			result.add(line);
		}
		return result;
	}

	public static List<String> smudge(List<String> pattern, int smudgeX, int smudgeY)
	{
		List<String> result = new ArrayList<>();
		for (int y = 0; y < sizeY(pattern); y++)
		{
			String line = "";
			for (int x = 0; x < sizeX(pattern); x++)
			{
				line += (x == smudgeX && y == smudgeY) ^ rock(pattern, x, y) ? "#" : ".";
			}
			result.add(line);
		}
		return result;
	}

	public static int sizeX(List<String> pattern)
	{
		return pattern.get(0).length();
	}

	public static int sizeY(List<String> pattern)
	{
		return pattern.size();
	}

	public static boolean rock(List<String> pattern, int x, int y)
	{
		return pattern.get(y).charAt(x) == '#';
	}

	public static boolean mirrorsY(List<String> pattern, int mirror)
	{
		boolean result = true;
		for (int test = 0; test < sizeY(pattern); test++)
		{
			if (0 <= mirror - test - 1 && mirror + test < sizeY(pattern))
			{
				String above = pattern.get(mirror - test - 1);
				String below = pattern.get(mirror + test);
				result &= above.equals(below);
			}
		}
		return result;
	}

	public static Integer mirrorsY(List<String> pattern, Integer skip)
	{
		Integer result = null;
		for (int mirror = 1; mirror < sizeY(pattern); mirror++)
		{
			if (mirrorsY(pattern, mirror) && (skip == null || skip != mirror))
			{
				result = mirror;
			}
		}
		return result;
	}

	public static Integer mirrorsYSmudge(List<String> pattern, Integer skip)
	{
		Integer result = null;
		for (int x = 0; x < sizeX(pattern); x++)
		{
			for (int y = 0; y < sizeY(pattern); y++)
			{
				List<String> patternSmudge = smudge(pattern, x, y);
				for (int mirror = 1; mirror < sizeY(patternSmudge); mirror++)
				{
					if (mirrorsY(patternSmudge, mirror) && (skip == null || skip != mirror))
					{
						result = mirror;
					}
				}
			}
		}
		return result;
	}

	public long part1()
	{
		long result = 0;
		for (List<String> pattern: this.patterns)
		{
			Integer mirror;
			mirror = mirrorsY(pattern, null);
			if (mirror != null)
			{
				result += 100L * mirror;
			}
			mirror = mirrorsY(transpose(pattern), null);
			if (mirror != null)
			{
				result += mirror;
			}
		}
		return result;
	}

	public long part2()
	{
		long result = 0;
		for (List<String> pattern: this.patterns)
		{
			Integer mirror;
			mirror = mirrorsY(pattern, null);
			mirror = mirrorsYSmudge(pattern, mirror);
			if (mirror != null)
			{
				result += 100L * mirror;
			}
			pattern = transpose(pattern);
			mirror = mirrorsY(pattern, null);
			mirror = mirrorsYSmudge(pattern, mirror);
			if (mirror != null)
			{
				result += mirror;
			}
		}
		return result;
	}

	public static void main(String[] args)
		throws ExceptionParserInput
	{
		Opgave13 opgave = new Opgave13(System.in);
		System.out.println(opgave.part2());
	}
}