package adventofcode;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;
import java.util.stream.Collectors;

public class Opgave9
	extends Opgave
{
	public final List<List<Long>> numbers;

	public Opgave9(InputStream in)
	{
		super(in);
		this.numbers = new ArrayList<>();
		for (String line: this.lines)
		{
			this.numbers.add
				(
					Arrays.stream(line.split("\\s+"))
						.map(Long::parseLong)
						.collect(Collectors.toList())
				);
		}
	}

	public Stack<List<Long>> stack(List<Long> numbers)
	{
		Stack<List<Long>> result = new Stack<>();
		while (numbers.stream().anyMatch(number -> number != 0))
		{
			result.push(numbers);
			List<Long> list = new ArrayList<>();
			for (int index = 0; index < numbers.size() - 1; index++)
			{
				list.add(numbers.get(index + 1) - numbers.get(index));
			}
			numbers = list;
		}
		return result;
	}

	public long predictLast(List<Long> numbers)
	{
		Stack<List<Long>> stack = stack(numbers);
		long result = 0;
		while (!stack.isEmpty())
		{
			List<Long> list = stack.pop();
			result += list.get(list.size() - 1);
		}
		return result;
	}

	public long predictFirst(List<Long> numbers)
	{
		Stack<List<Long>> stack = stack(numbers);
		long result = 0;
		while (!stack.isEmpty())
		{
			List<Long> list = stack.pop();
			result = list.get(0) - result;
		}
		return result;
	}

	public long part1()
	{
		return this.numbers
			.stream()
			.map(this::predictLast)
			.reduce(0L, Long::sum);
	}

	public long part2()
	{
		return this.numbers
			.stream()
			.map(this::predictFirst)
			.reduce(0L, Long::sum);
	}

	public static void main(String[] args)
	{
		Opgave9 opgave9 = new Opgave9(System.in);
		System.out.println(opgave9.part1());
		System.out.println(opgave9.part2());
	}
}
