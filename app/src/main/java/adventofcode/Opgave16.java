package adventofcode;

import java.io.InputStream;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public class Opgave16
	extends Opgave
{
	abstract class Tile
	{
		public final Set<Vector2DInt> incomings;

		public Tile()
		{
			this.incomings = new TreeSet<>();
		}

		public Set<Vector2DInt> outgoing()
		{
			final Set<Vector2DInt> result = new TreeSet<>();
			this.incomings.forEach(incoming -> result.addAll(map().get(incoming)));
			return result;
		}

		public boolean energized()
		{
			return !outgoing().isEmpty();
		}

		public abstract Map<Vector2DInt, Set<Vector2DInt>> map();

		public abstract Tile copy();
	}

	class TilePlain
		extends Tile
	{
		@Override
		public Map<Vector2DInt, Set<Vector2DInt>> map()
		{
			return Map.of
				(
					Vector2DInt.NORTH, Set.of(Vector2DInt.NORTH),
					Vector2DInt.SOUTH, Set.of(Vector2DInt.SOUTH),
					Vector2DInt.WEST, Set.of(Vector2DInt.WEST),
					Vector2DInt.EAST, Set.of(Vector2DInt.EAST)
				);
		}

		@Override
		public Tile copy()
		{
			return new TilePlain();
		}
	}

	class TileSplitterNorthSouth
		extends Tile
	{
		@Override
		public Map<Vector2DInt, Set<Vector2DInt>> map()
		{
			return Map.of
				(
					Vector2DInt.NORTH, Set.of(Vector2DInt.NORTH),
					Vector2DInt.SOUTH, Set.of(Vector2DInt.SOUTH),
					Vector2DInt.WEST, Set.of(Vector2DInt.NORTH, Vector2DInt.SOUTH),
					Vector2DInt.EAST, Set.of(Vector2DInt.NORTH, Vector2DInt.SOUTH)
				);
		}

		@Override
		public Tile copy()
		{
			return new TileSplitterNorthSouth();
		}
	}

	class TileSplitterWestEast
		extends Tile
	{
		@Override
		public Map<Vector2DInt, Set<Vector2DInt>> map()
		{
			return Map.of
				(
					Vector2DInt.NORTH, Set.of(Vector2DInt.WEST, Vector2DInt.EAST),
					Vector2DInt.SOUTH, Set.of(Vector2DInt.WEST, Vector2DInt.EAST),
					Vector2DInt.WEST, Set.of(Vector2DInt.WEST),
					Vector2DInt.EAST, Set.of(Vector2DInt.EAST)
				);
		}

		@Override
		public Tile copy()
		{
			return new TileSplitterWestEast();
		}
	}

	class TileSplitterMirrorSlash
		extends Tile
	{
		@Override
		public Map<Vector2DInt, Set<Vector2DInt>> map()
		{
			return Map.of
				(
					Vector2DInt.NORTH, Set.of(Vector2DInt.EAST),
					Vector2DInt.SOUTH, Set.of(Vector2DInt.WEST),
					Vector2DInt.WEST, Set.of(Vector2DInt.SOUTH),
					Vector2DInt.EAST, Set.of(Vector2DInt.NORTH)
				);
		}

		@Override
		public Tile copy()
		{
			return new TileSplitterMirrorSlash();
		}
	}

	class TileSplitterMirrorBackslash
		extends Tile
	{
		@Override
		public Map<Vector2DInt, Set<Vector2DInt>> map()
		{
			return Map.of
				(
					Vector2DInt.NORTH, Set.of(Vector2DInt.WEST),
					Vector2DInt.SOUTH, Set.of(Vector2DInt.EAST),
					Vector2DInt.WEST, Set.of(Vector2DInt.NORTH),
					Vector2DInt.EAST, Set.of(Vector2DInt.SOUTH)
				);
		}

		@Override
		public Tile copy()
		{
			return new TileSplitterMirrorBackslash();
		}
	}

	public final int sizeX;
	public final int sizeY;
	public final Tile[][] tiles;

	public Opgave16(InputStream in)
	{
		super(in);
		this.sizeX = this.lines.get(0).length();
		this.sizeY = this.lines.size();
		this.tiles = new Tile[this.sizeX + 2][this.sizeY + 2];
		for (int y = 0; y < this.sizeY + 2; y++)
		{
			for (int x = 0; x < this.sizeX + 2; x++)
			{
				this.tiles[x][y] = new TilePlain();
			}
		}
		for (int y = 0; y < this.sizeY; y++)
		{
			for (int x = 0; x < this.sizeX; x++)
			{
				Tile tile;
				switch (this.lines.get(y).charAt(x))
				{
					case '.':
						tile = new TilePlain();
						break;
					case '|':
						tile = new TileSplitterNorthSouth();
						break;
					case '-':
						tile = new TileSplitterWestEast();
						break;
					case '/':
						tile = new TileSplitterMirrorSlash();
						break;
					case '\\':
						tile = new TileSplitterMirrorBackslash();
						break;
					default:
						tile = new TilePlain();
				}
				this.tiles[x + 1][y + 1] = tile;
			}
		}
	}

	public Tile[][] copy()
	{
		Tile[][] result = new Tile[this.sizeX + 2][this.sizeY + 2];
		for (int x = 0; x < this.sizeX + 2; x++)
		{
			for (int y = 0; y < this.sizeY + 2; y++)
			{
				result[x][y] = this.tiles[x][y].copy();
			}
		}
		return result;
	}

	public void propagate(Tile[][] result)
	{
		boolean changed = true;
		while (changed)
		{
			changed = false;
			for (int x = 1; x <= this.sizeX; x++)
			{
				for (int y = 1; y <= this.sizeY; y++)
				{
					Tile tile = result[x][y];
					for (Vector2DInt outgoing: tile.outgoing())
					{
						Tile tileNext = result[x + outgoing.x][y + outgoing.y];
						changed |= tileNext.incomings.add(outgoing);
					}
				}
			}
		}
	}

	public long energized(Tile[][] tiles)
	{
		long result = 0;
		for (int x = 1; x <= this.sizeX; x++)
		{
			for (int y = 1; y <= this.sizeY; y++)
			{
				Tile tile = tiles[x][y];
				if (tile.energized())
				{
					result += 1;
				}
			}
		}
		return result;
	}

	public long part1()
	{
		Tile[][] tiles = copy();
		tiles[1][1].incomings.add(Vector2DInt.EAST);
		propagate(tiles);
		return energized(tiles);
	}

	public long part2()
	{
		long result = 0;
		for (int y = 1; y <= this.sizeY; y++)
		{
			Tile[][] tiles = copy();
			tiles[1][y].incomings.add(Vector2DInt.EAST);
			propagate(tiles);
			result = Math.max(result, energized(tiles));
		}
		for (int y = 1; y <= this.sizeY; y++)
		{
			Tile[][] tiles = copy();
			tiles[this.sizeX][y].incomings.add(Vector2DInt.WEST);
			propagate(tiles);
			result = Math.max(result, energized(tiles));
		}
		for (int x = 1; x <= this.sizeX; x++)
		{
			Tile[][] tiles = copy();
			tiles[x][1].incomings.add(Vector2DInt.SOUTH);
			propagate(tiles);
			result = Math.max(result, energized(tiles));
		}
		for (int x = 1; x <= this.sizeX; x++)
		{
			Tile[][] tiles = copy();
			tiles[x][this.sizeY].incomings.add(Vector2DInt.NORTH);
			propagate(tiles);
			result = Math.max(result, energized(tiles));
		}
		return result;
	}

	public static void main(String[] args)
	{
		Opgave16 opgave = new Opgave16(System.in);
		System.out.println(opgave.part1());
		System.out.println(opgave.part2());
	}
}