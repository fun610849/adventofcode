package adventofcode;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Opgave
{
	public static InputStream open(String name)
	{
		final ClassLoader loader = Opgave.class.getClassLoader();
		return loader.getResourceAsStream(name);
	}

	public final List<String> lines;
	public final String input;

	public Opgave(InputStream in)
	{
		final Scanner scanner = new Scanner(in);
		StringBuilder stringBuilder = new StringBuilder();
		this.lines = new ArrayList<>();
		while (scanner.hasNextLine())
		{
			String line = scanner.nextLine();
			this.lines.add(line);
			stringBuilder.append(line);
		}
		this.input = stringBuilder.toString();
	}
}