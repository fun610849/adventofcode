package adventofcode;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

abstract class Instruction
{
	public static Instruction instruction(String instruction)
	{
		Instruction result;
		if (instruction.contains("="))
		{
			String[] parts = instruction.split("=");
			result = new InstructionAdd(parts[0], Integer.parseInt(parts[1]));
		}
		else
		{
			result = new InstructionRemove(instruction.split("-")[0]);
		}
		return result;
	}

	public final String label;

	public Instruction(String label)
	{
		this.label = label;
	}

	public Box box(Box[] boxes)
	{
		return boxes[Opgave15.hash(this.label)];
	}

	public abstract void execute(Box[] boxes);

	@Override
	public String toString()
	{
		return this.label;
	}
}

class InstructionRemove
	extends Instruction
{
	public InstructionRemove(String label)
	{
		super(label);
	}

	@Override
	public void execute(Box[] boxes)
	{
		final Box box = box(boxes);
		final Optional<Lens> lens =
			box
				.stream()
				.filter(l -> l.label.equals(this.label))
				.findAny();
		lens.ifPresent(l -> box.remove(l));
	}

	@Override
	public String toString()
	{
		return super.toString() + "-";
	}
}

class InstructionAdd
	extends Instruction
{
	public final int focal;

	public InstructionAdd(String label, int focal)
	{
		super(label);
		this.focal = focal;
	}

	@Override
	public void execute(Box[] boxes)
	{
		final Box box = box(boxes);
		final Optional<Lens> lens =
			box
				.stream()
				.filter(l -> l.label.equals(this.label))
				.findAny();
		if (lens.isEmpty())
		{
			box.add(new Lens(this.label, this.focal));
		}
		else
		{
			lens.get().focal = this.focal;
		}
	}

	@Override
	public String toString()
	{
		return super.toString() + "=" + this.focal;
	}
}

class Lens
	implements Comparable<Lens>
{
	public final String label;
	public int focal;

	public Lens(String label, int focal)
	{
		this.label = label;
		this.focal = focal;
	}

	@Override
	public int compareTo(Lens that)
	{
		return this.label.compareTo(that.label);
	}
}

class Box
	extends ArrayList<Lens>
{
}

public class Opgave15
	extends Opgave
{
	public final List<Instruction> instructions;

	public Opgave15(InputStream in)
	{
		super(in);
		this.instructions = new ArrayList<>();
		this.lines
			.stream()
			.map(line -> line.split(","))
			.forEach
				(
					instructions ->
						Arrays.
							stream(instructions)
							.forEach
								(
									instruction -> this.instructions.add(Instruction.instruction(instruction))
								)
				);
	}

	public static int hash(String string)
	{
		int result = 0;
		for (char character: string.toCharArray())
		{
			result += character;
			result *= 17;
			result %= 256;
		}
		return result;
	}

	public long part1()
	{
		long result = 0;
		for (Instruction instruction: this.instructions)
		{
			result += hash(instruction.toString());
		}
		return result;
	}

	public long part2()
	{
		long result = 0;
		Box[] boxes = new Box[256];
		for (int index = 0; index < 256; index++)
		{
			boxes[index] = new Box();
		}
		for (Instruction instruction: this.instructions)
		{
			instruction.execute(boxes);
		}
		for (int indexBox = 0; indexBox < boxes.length; indexBox++)
		{
			Box box = boxes[indexBox];
			for (int indexLens = 0; indexLens < box.size(); indexLens++)
			{
				Lens lens = box.get(indexLens);
				result += (long) (indexBox + 1) * (indexLens + 1) * lens.focal;
			}
		}
		return result;
	}

	public static void main(String[] args)
	{
		Opgave15 opgave15 = new Opgave15(System.in);
		System.out.println(opgave15.part1());
		System.out.println(opgave15.part2());
	}
}
