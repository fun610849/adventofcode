package adventofcode;

import java.io.InputStream;
import java.util.Map;
import java.util.TreeMap;

public class Opgave14
	extends Opgave
{
	public final int sizeX;
	public final int sizeY;
	public final boolean[][] rocksFixed;
	public final boolean[][] rocksRound;

	public Opgave14(InputStream in)
	{
		super(in);
		this.sizeX = this.lines.get(0).length();
		this.sizeY = this.lines.size();
		this.rocksFixed = new boolean[this.sizeX][this.sizeY];
		this.rocksRound = new boolean[this.sizeX][this.sizeY];
		for (int y = 0; y < this.sizeY; y++)
		{
			String line = this.lines.get(y);
			for (int x = 0; x < this.sizeX; x++)
			{
				char character = line.charAt(x);
				if (character == '#')
				{
					this.rocksFixed[x][y] = true;
				}
				if (character == 'O')
				{
					this.rocksRound[x][y] = true;
				}
			}
		}
	}

	public void rollNorth()
	{
		for (int x = 0; x < this.sizeX; x++)
		{
			int lastFree = 0;
			for (int y = 0; y < this.sizeY; y++)
			{
				if (this.rocksFixed[x][y])
				{
					lastFree = y + 1;
				}
				else
				{
					if (this.rocksRound[x][y])
					{
						this.rocksRound[x][y] = false;
						this.rocksRound[x][lastFree] = true;
						lastFree += 1;
					}
				}
			}
		}
	}

	public void rollWest()
	{
		for (int y = 0; y < this.sizeY; y++)
		{
			int lastFree = 0;
			for (int x = 0; x < this.sizeX; x++)
			{
				if (this.rocksFixed[x][y])
				{
					lastFree = x + 1;
				}
				else
				{
					if (this.rocksRound[x][y])
					{
						this.rocksRound[x][y] = false;
						this.rocksRound[lastFree][y] = true;
						lastFree += 1;
					}
				}
			}
		}
	}

	public void rollSouth()
	{
		for (int x = 0; x < this.sizeX; x++)
		{
			int lastFree = this.sizeY - 1;
			for (int y = this.sizeY - 1; 0 <= y; y--)
			{
				if (this.rocksFixed[x][y])
				{
					lastFree = y - 1;
				}
				else
				{
					if (this.rocksRound[x][y])
					{
						this.rocksRound[x][y] = false;
						this.rocksRound[x][lastFree] = true;
						lastFree -= 1;
					}
				}
			}
		}
	}

	public void rollEast()
	{
		for (int y = 0; y < this.sizeY; y++)
		{
			int lastFree = this.sizeX - 1;
			for (int x = this.sizeX - 1; 0 <= x; x--)
			{
				if (this.rocksFixed[x][y])
				{
					lastFree = x - 1;
				}
				else
				{
					if (this.rocksRound[x][y])
					{
						this.rocksRound[x][y] = false;
						this.rocksRound[lastFree][y] = true;
						lastFree -= 1;
					}
				}
			}
		}
	}

	public void cycle()
	{
		rollNorth();
		rollWest();
		rollSouth();
		rollEast();
	}

	public long load()
	{
		long result = 0;
		for (int x = 0; x < this.sizeX; x++)
		{
			for (int y = 0; y < this.sizeY; y++)
			{
				if (this.rocksRound[x][y])
				{
					result += this.sizeY - y;
				}
			}
		}
		return result;
	}

	public void print()
	{
		for (int y = 0; y < this.sizeY; y++)
		{
			for (int x = 0; x < this.sizeX; x++)
			{
				System.out.print(this.rocksFixed[x][y] ? '#' : this.rocksRound[x][y] ? 'O' : '.');
			}
			System.out.println();
		}
		System.out.println();
	}

	public long part1()
	{
		rollNorth();
		return load();
	}

	public boolean[][] copy()
	{
		boolean[][] result = new boolean[this.sizeX][this.sizeY];
		for (int y = 0; y < this.sizeY; y++)
		{
			for (int x = 0; x < this.sizeX; x++)
			{
				result[x][y] = this.rocksRound[x][y];
			}
		}
		return result;
	}

	public long part2()
	{
		int cycles = 1000000000;
		Map<boolean[][], Integer> map = new TreeMap<>
			(
				(t0, t1) ->
				{
					int result = 0;
					for (int x = 0; result == 0 && x < Opgave14.this.sizeX; x++)
					{
						for (int y = 0; result == 0 && y < Opgave14.this.sizeY; y++)
						{
							result = Boolean.compare(t0[x][y], t1[x][y]);
						}
					}
					return result;
				}
			);
		int cycle = 0;
		while (cycle < cycles && !map.containsKey(this.rocksRound))
		{
			map.put(copy(), cycle);
			cycle();
			cycle += 1;
		}
		Integer previous = map.get(this.rocksRound);
		if (previous != null)
		{
			int period = cycle - previous;
			cycle += period * ((cycles - cycle) / period);
		}
		while (cycle < cycles)
		{
			cycle();
			cycle += 1;
		}
		return load();
	}

	public static void main(String[] args)
	{
		Opgave14 opgave = new Opgave14(System.in);
		System.out.println(opgave.part2());
	}
}