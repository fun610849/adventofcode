package adventofcode;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

class Galaxy
{
	int x;
	int y;

	public Galaxy(int x, int y)
	{
		this.x = x;
		this.y = y;
	}

	public int distance(Galaxy galaxy)
	{
		return Math.abs(this.x - galaxy.x) + Math.abs(this.y - galaxy.y);
	}
}

public class Opgave11
	extends Opgave
{
	public int sizeX;
	public int sizeY;
	public List<Galaxy> galaxies;

	public Opgave11(InputStream in)
	{
		super(in);
		this.sizeX = this.lines.get(0).length();
		this.sizeY = this.lines.size();
		this.galaxies = new ArrayList<>();
		for (int x = 0; x < this.sizeX; x++)
		{
			for (int y = 0; y < this.sizeY; y++)
			{
				if (this.lines.get(y).charAt(x) == '#')
				{
					this.galaxies.add(new Galaxy(x, y));
				}
			}
		}
	}

	public boolean emptyX(int x)
	{
		boolean result = true;
		for (Galaxy galaxy: this.galaxies)
		{
			result &= galaxy.x != x;
		}
		return result;
	}

	public boolean emptyY(int y)
	{
		boolean result = true;
		for (Galaxy galaxy: this.galaxies)
		{
			result &= galaxy.y != y;
		}
		return result;
	}

	public void expandX(int delta)
	{
		for (int index = 0; index < this.sizeX; index++)
		{
			if (emptyX(index))
			{
				for (Galaxy galaxy: this.galaxies)
				{
					if (index < galaxy.x)
					{
						galaxy.x += delta;
					}
				}
				index += delta;
				this.sizeX += delta;
			}
		}
	}

	public void expandY(int delta)
	{
		for (int index = 0; index < this.sizeX; index++)
		{
			if (emptyY(index))
			{
				for (Galaxy galaxy: this.galaxies)
				{
					if (index < galaxy.y)
					{
						galaxy.y += delta;
					}
				}
				index += delta;
				this.sizeY += delta;
			}
		}
	}

	public long expandAndDistance(int delta)
	{
		expandX(delta);
		expandY(delta);
		long result = 0;
		for (Galaxy galaxy0: this.galaxies)
		{
			for (Galaxy galaxy1: this.galaxies)
			{
				result += galaxy0.distance(galaxy1);
			}
		}
		return result / 2;
	}

	public long part1()
	{
		return expandAndDistance(1);
	}

	public long part2()
	{
		return expandAndDistance(1000000 - 1);
	}

	public static void main(String[] args)
	{
		Opgave11 opgave = new Opgave11(System.in);
		System.out.println(opgave.part2());
	}
}
