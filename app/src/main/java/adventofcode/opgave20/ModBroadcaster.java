package adventofcode.opgave20;

import java.util.LinkedList;

class ModBroadcaster
	extends Mod
{
	public ModBroadcaster()
	{
		super(Opgave20.BROADCASTER_NAME);
	}

	@Override
	public void pulse(Pulse pulse, LinkedList<Pulse> pulses)
	{
		pulses(pulse.high, pulses);
	}

	@Override
	public String name()
	{
		return "&" + this.name;
	}

	@Override
	public String toString()
	{
		return super.toString() + ")";
	}
}
