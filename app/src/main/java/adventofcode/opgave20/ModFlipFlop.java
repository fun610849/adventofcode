package adventofcode.opgave20;

import java.util.LinkedList;

class ModFlipFlop
	extends Mod
{
	public boolean on;

	public ModFlipFlop(String name)
	{
		super(name);
		this.on = false;
	}

	@Override
	public void pulse(Pulse pulse, LinkedList<Pulse> pulses)
	{
		if (!pulse.high)
		{
			this.on = !this.on;
			pulses(this.on, pulses);
		}
	}

	@Override
	public String name()
	{
		return "%" + this.name;
	}

	@Override
	public String toString()
	{
		return super.toString() + String.format(", on: '%b')", this.on);
	}
}
