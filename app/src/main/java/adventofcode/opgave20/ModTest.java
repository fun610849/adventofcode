package adventofcode.opgave20;

import java.util.LinkedList;

class ModTest
	extends Mod
{
	public ModTest(String name)
	{
		super(name);
	}

	@Override
	public void pulse(Pulse pulse, LinkedList<Pulse> pulses)
	{
	}

	@Override
	public String name()
	{
		return this.name;
	}

	@Override
	public String toString()
	{
		return super.toString() + ")";
	}
}
