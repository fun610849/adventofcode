package adventofcode.opgave20;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

abstract class Mod
	implements Comparable<Mod>
{
	public final String name;
	public final List<Mod> ins;
	public final List<Mod> outs;
	public final HighLow highLow;

	public Mod(String name)
	{
		this.name = name;
		this.ins = new ArrayList<>();
		this.outs = new ArrayList<>();
		this.highLow = new HighLow();
	}

	@Override
	public int compareTo(Mod that)
	{
		return this.name.compareTo(that.name);
	}

	public void pulses(final boolean high, final LinkedList<Pulse> pulses)
	{
		for (Mod out: this.outs)
		{
			pulses.addLast(new Pulse(this, out, high));
		}
	}

	public abstract void pulse(Pulse pulse, LinkedList<Pulse> pulses);

	public void pulseCount(Pulse pulse, LinkedList<Pulse> pulses)
	{
		if (pulse.high)
		{
			this.highLow.high += 1;
		}
		else
		{
			this.highLow.low += 1;
		}
		pulse(pulse, pulses);
	}

	public abstract String name();
	@Override
	public String toString()
	{
//		return String.format("%s(name: '%s', high: '%d', low: '%d'", this.getClass().getSimpleName(), this.name, this.highLow.high, this.highLow.low);
		return String.format("%s(name: '%s'", this.getClass().getSimpleName(), name());
	}
}
