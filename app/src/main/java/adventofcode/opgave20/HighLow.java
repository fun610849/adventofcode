package adventofcode.opgave20;

class HighLow
{
	public long high;
	public long low;

	public HighLow(long high, long low)
	{
		this.high = high;
		this.low = low;
	}

	public HighLow()
	{
		this(0, 0);
	}

	public HighLow add(HighLow that)
	{
		return new HighLow(this.high + that.high, this.low + that.low);
	}

	public HighLow subtract(HighLow that)
	{
		return new HighLow(this.high - that.high, this.low - that.low);
	}

	public long total()
	{
		return this.high + this.low;
	}

	@Override
	public String toString()
	{
		return String.format("high: %6d, low: %6d, total: %6d", this.high, this.low, this.high + this.low);
	}
}
