package adventofcode.opgave20;

class Pulse
{
	public final Mod from;
	public final Mod to;
	public final boolean high;

	public Pulse(Mod from, Mod to, boolean high)
	{
		this.from = from;
		this.to = to;
		this.high = high;
	}

	@Override
	public String toString()
	{
		return String.format("Pulse(%s -> %s (%b))", this.from.name(), this.to.name(), this.high);
	}
}
