package adventofcode.opgave20;

import nl.novit.util.Util;

import java.util.LinkedList;
import java.util.Set;
import java.util.TreeSet;

class ModConjunction
	extends Mod
{
	public final Set<Mod> high;

	public ModConjunction(String name)
	{
		super(name);
		this.high = new TreeSet<>();
	}

	@Override
	public void pulse(Pulse pulse, LinkedList<Pulse> pulses)
	{
		if (pulse.high)
		{
			this.high.add(pulse.from);
		}
		else
		{
			this.high.remove(pulse.from);
		}
		pulses(this.high.size() != this.ins.size(), pulses);
	}

	@Override
	public String name()
	{
		return "&" + this.name;
	}

	@Override
	public String toString()
	{
		return super.toString() + String.format(", on: '%s')", Util.toString(this.high, ", ", mod -> mod.name()));
	}
}
