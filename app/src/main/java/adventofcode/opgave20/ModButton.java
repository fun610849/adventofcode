package adventofcode.opgave20;

import java.util.LinkedList;

class ModButton
	extends Mod
{
	public ModButton(ModBroadcaster broadcaster)
	{
		super(Opgave20.BUTTON_NAME);
		this.outs.add(broadcaster);
		broadcaster.ins.add(this);
	}

	@Override
	public void pulse(Pulse pulse, LinkedList<Pulse> pulses)
	{
		throw new UnsupportedOperationException();
	}

	public LinkedList<Pulse> push()
	{
		final LinkedList<Pulse> result = new LinkedList<>();
		pulses(false, result);
		return result;
	}

	@Override
	public String name()
	{
		return  this.name;
	}

	@Override
	public String toString()
	{
		return super.toString() + ")";
	}
}
