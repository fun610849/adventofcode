package adventofcode.opgave20;

import java.util.LinkedList;

public class ModFlopFlip
	extends Mod
{
	public int status;

	public ModFlopFlip(Mod first, Mod second)
	{
		super(first.name + second.name);
		this.status = 0;
		for (Mod mod: first.ins)
		{
			mod.outs.remove(first);
			mod.outs.add(this);
			this.ins.add(mod);
		}
		for (Mod mod: second.outs)
		{
			mod.ins.remove(second);
			mod.ins.add(this);
			this.outs.add(mod);
		}
		first.ins.clear();
		first.outs.clear();
		second.ins.clear();
		second.outs.clear();
	}

	@Override
	public void pulse(Pulse pulse, LinkedList<Pulse> pulses)
	{
		if (!pulse.high)
		{
			switch (this.status)
			{
				case 0:
					this.status = 2;
					this.highLow.high += 1;
					break;
				case 1:
					this.status = 3;
					this.highLow.high += 1;
					break;
				case 2:
					this.status = 1;
					this.highLow.low += 1;
					pulses(true, pulses);
					break;
				case 3:
					this.status = 0;
					this.highLow.low += 1;
					pulses(false, pulses);
					break;
			}
		}
	}

	@Override
	public String name()
	{
		return "?" + this.name;
	}

	@Override
	public String toString()
	{
		return super.toString() + String.format(", status: %d)", 8);
	}
}
