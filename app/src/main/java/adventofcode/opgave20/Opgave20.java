package adventofcode.opgave20;

import adventofcode.Opgave;
import nl.novit.util.Util;

import java.io.InputStream;
import java.util.*;
import java.util.stream.Collectors;

public class Opgave20
	extends Opgave
{
	public static final String BROADCASTER_NAME = "broadcaster";
	public static final String BUTTON_NAME = "button";
	public final ModBroadcaster broadcaster = new ModBroadcaster();
	public final ModButton button = new ModButton(broadcaster);
	public ModTest test;
	public final Map<String, Mod> mods;

	public static String substring(String name)
	{
		return name.equals(BROADCASTER_NAME) ? BROADCASTER_NAME : name.substring(1);
	}

	public Opgave20(InputStream in)
	{
		super(in);
		this.mods = new TreeMap<>();
		for (String line: this.lines)
		{
			String name = line.split(" -> ")[0];
			if (name.equals(BROADCASTER_NAME))
			{
				this.mods.put(BROADCASTER_NAME, broadcaster);
			}
			else
			{
				if (name.startsWith("%"))
				{
					name = substring(name);
					this.mods.put(name, new ModFlipFlop(name));
				}
				if (name.startsWith("&"))
				{
					name = substring(name);
					this.mods.put(name, new ModConjunction(name));
				}
			}
		}
		for (String line: this.lines)
		{
			String name = substring(line.split(" -> ")[0]);
			Mod mod = this.mods.get(name);
			String[] namesOut = line.split(" -> ")[1].split(",\\s*");
			for (String nameOut: namesOut)
			{
				Mod out = this.mods.get(nameOut);
				if (out == null)
				{
					this.test = new ModTest(nameOut);
					out = test;
					this.mods.put(nameOut, out);
				}
				mod.outs.add(out);
				out.ins.add(mod);
			}
		}
//		simplify();
	}

	public void simplify()
	{
		List<ModFlopFlip> flopFlips = new ArrayList<>();
		for (Mod second: this.mods.values())
		{
			if (second instanceof ModFlipFlop)
			{
				if (second.ins.size() == 1)
				{
					Mod first = second.ins.get(0);
					if (first.outs.size() == 1)
					{
						flopFlips.add(new ModFlopFlip(first, second));
					}
				}
			}
		}
		flopFlips.forEach(mod -> this.mods.put(mod.name, mod));
	}

	public HighLow sum()
	{
		return this.mods.values().stream()
			.map(mod -> mod.highLow)
			.reduce(new HighLow(), HighLow::add);
	}

	long result = 0;
	public void process(LinkedList<Pulse> pulses)
	{
		Map< Integer, Mod> order  = new TreeMap<>();
		result += 1;
		if (result % 4021 == 0)
		{
			System.out.println("next");
		}
		while (!pulses.isEmpty())
		{
			Pulse pulse = pulses.removeFirst();
			if (pulse.to instanceof ModConjunction)
			{
				if (!order.containsValue(pulse.to))
				{
					order.put( order.size(), pulse.to);
				}
				if (result % 4021 == 0 && pulse.to.name.equals("cq"))
				{
					System.out.println(pulse);
				}
			}
			pulse.to.pulseCount(pulse, pulses);
		}
		List<Mod> list = new ArrayList<>(order.values());
		List<Mod> check = List.of
			(
				this.mods.get("zq"),
				this.mods.get("mt"),
				this.mods.get("zd"),
				this.mods.get("kx"),
				this.mods.get("qz"),
				this.mods.get("tt"),
				this.mods.get("jx"),
				this.mods.get("cq"),
				this.mods.get("qn")
			);
			if (Util.compareIterable(check, list) != 0)
			{
				System.exit(-1);
			}
//		System.out.println(order);
	}

	public static Set<Mod> test(LinkedList<Pulse> pulses, boolean high)
	{
		return pulses.stream()
			.filter(pulse -> pulse.high == high)
			.map(pulse -> pulse.to)
			.filter(mod -> mod instanceof ModConjunction)
			.collect(Collectors.toSet());

	}
	public void processBatch(LinkedList<Pulse> pulses)
	{
		while (!pulses.isEmpty())
		{
			Set<Mod> high = test(pulses, true);
			Set<Mod> low = test(pulses, false);
			Set<Mod> all = new TreeSet<>(high);
			all.addAll(low);
			if (high.size() + low.size() != all.size())
			{
				System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!");
			}
			LinkedList<Pulse> next = new LinkedList<>();
			while (!pulses.isEmpty())
			{
				Pulse pulse = pulses.removeLast();
				pulse.to.pulseCount(pulse, next);
			}
			pulses = next;
		}
	}

	public void pushButton()
	{
		LinkedList<Pulse> pulses = button.push();
		process(pulses);
	}

	public HighLow solvePart1(int n)
	{
		for (int index = 0; index < n; index++)
		{
			pushButton();
		}
		return sum();
	}

	public long part1()
	{
		HighLow highLow = solvePart1(1000);
		return highLow.high * highLow.low;
	}

	public boolean test(String name)
	{
		ModConjunction  mod = (ModConjunction) this.mods.get(name);
		return mod.high.size() == mod.ins.size();
	}
	public long part2()
	{
		String name = "tt";
		int period = 3931;
		long result = 0;
		while (this.test.highLow.low == 0)
		{
			this.test.highLow.high = 0;
			this.test.highLow.low = 0;
			this.mods.get(name).highLow.high = 0;
			this.mods.get(name).highLow.low = 0;
			pushButton();
			result += 1;
//			if (test("kx"))
//			{
//				System.out.println("kx:" + result);
//			}
			if (1 < this.mods.get(name).highLow.low)
			{
				if (this.mods.get(name).highLow.high != 0)
				{
					System.out.printf("%s: %d%n", name, result);
					System.exit(0);
				}
			}
			if (result % 10000000 == 0)
			{
				System.out.printf("result: %10d, high: %d%n", result, this.test.highLow.high);
			}
			if (this.test.highLow.high < 4)
			{
				System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
			}
		}
		return result;
	}

	public static void main(String[] args)
	{
		Opgave20 opgave = new Opgave20(System.in);
		System.out.println(opgave.part1());
//		System.out.println(opgave.part2());
	}
}