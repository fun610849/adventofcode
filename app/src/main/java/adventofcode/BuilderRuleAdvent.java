package adventofcode;

import nl.novit.parser.rule.Rule;
import nl.novit.parser.util.BuilderRule;

import java.util.List;

public abstract class BuilderRuleAdvent<Type>
	extends BuilderRule<Character, Type>
{
	public final Rule<Character, Integer> ruleDigit;
	public final Rule<Character, List<Integer>> ruleDigits;
	public final Rule<Character, Integer> ruleInteger;

	public BuilderRuleAdvent()
	{
		this.ruleDigit =
			union
				(
					terminal('0', 0),
					terminal('1', 1),
					terminal('2', 2),
					terminal('3', 3),
					terminal('4', 4),
					terminal('5', 5),
					terminal('6', 6),
					terminal('7', 7),
					terminal('8', 8),
					terminal('9', 9)
				);
		this.ruleDigits = repetition(this.ruleDigit);
		this.ruleInteger =
			identity1
				(
					digits -> digits.stream().reduce(0, (x, y) -> 10 * x + y),
					this.ruleDigits
				);
	}
}