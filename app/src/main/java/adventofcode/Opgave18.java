package adventofcode;

import nl.novit.parser.exception.ExceptionParserInput;
import nl.novit.parser.parser.ParserLL1;
import nl.novit.parser.rule.Rule;
import nl.novit.parser.util.StringParser;
import nl.novit.parser.util.StringParserDefault;
import nl.novit.util.Tuple2;

import java.io.InputStream;
import java.math.BigInteger;
import java.util.*;

class Edge18
	implements Comparable<Edge18>
{
	public final Vector2DInt start;
	public final Vector2DInt end;

	public Edge18(Vector2DInt start, Vector2DInt end)
	{
		this.start = start;
		this.end = end;
	}

	@Override
	public int compareTo(Edge18 that)
	{
		int result = this.start.compareTo(that.start);
		if (result == 0)
		{
			result = this.end.compareTo(that.end);
		}
		return result;
	}

	public Edge18 inverse()
	{
		return new Edge18(this.end, this.start);
	}

	public boolean isHorizontal()
	{
		return this.start.y == this.end.y;
	}

	public int minimumX()
	{
		return Math.min(this.start.x, this.end.x);
	}

	public boolean isAdjacent(Edge18 that)
	{
		return this.start.equals(that.end) || this.end.equals(that.start);
	}

	@Override
	public String toString()
	{
		return "(" + this.start + " -> " + this.end + ")";
	}
}

class Dig
{
	public final Vector2DInt direction;
	public final int distance;
	public final List<Character> color;

	public Dig(Vector2DInt direction, int distance, List<Character> color)
	{
		this.direction = direction;
		this.distance = distance;
		this.color = color;
	}

	public Vector2DInt directionPart2()
	{
		return Opgave18.DIRECTIONS_PART2.get(color.get(5) - 48);
	}
}

public class Opgave18
	extends Opgave
{
	public static final Map<Character, Vector2DInt> DIRECTIONS_PART1 = Map.of
		(
			'U', Vector2DInt.NORTH,
			'D', Vector2DInt.SOUTH,
			'L', Vector2DInt.WEST,
			'R', Vector2DInt.EAST
		);
	public static final Map<Integer, Vector2DInt> DIRECTIONS_PART2 = Map.of
		(
			0, Vector2DInt.EAST,
			1, Vector2DInt.SOUTH,
			2, Vector2DInt.WEST,
			3, Vector2DInt.NORTH
		);
	public final List<Dig> digs;

	public Opgave18(InputStream in)
	{
		super(in);
		StringParser<Dig> parser = new StringParserDefault<>
			(
				new ParserLL1<>
					(
						new BuilderRuleAdvent<Dig>()
						{
							@Override
							public Rule<Character, Dig> rule()
							{
								return
									identity3
										(
											Dig::new,
											concatenation111
												(
													identity1
														(
															DIRECTIONS_PART1::get,
															union
																(
																	terminal('U'),
																	terminal('D'),
																	terminal('L'),
																	terminal('R')
																)
														),
													concatenation01
														(
															dummy
																(
																	terminal(' ')
																),
															this.ruleInteger
														),
													concatenation01
														(
															dummy
																(
																	concatenationTerminals(List.of(' ', '(', '#'))
																),
															concatenation10
																(
																	repetition
																		(
																			unionTerminals(List.of('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'))
																		),
																	dummy(terminal(')'))
																)
														)
												)
										);
							}
						}.rule()
					)
			);
		this.digs = new ArrayList<>();
		for (String line: this.lines)
		{
			try
			{
				this.digs.add(parser.parse(line).getTarget());
			}
			catch (ExceptionParserInput exception)
			{
				throw new RuntimeException(exception);
			}
		}
	}

	public static Tuple2<Vector2DInt, Vector2DInt> bbox(SortedSet<Edge18> edges)
	{
		Vector2DInt start = new Vector2DInt(0, 0);
		Vector2DInt end = new Vector2DInt(0, 0);
		for (Edge18 edge: edges)
		{
			start = new Vector2DInt(Math.min(start.x, edge.start.x), Math.min(start.y, edge.start.y));
			start = new Vector2DInt(Math.min(start.x, edge.end.x), Math.min(start.y, edge.end.y));
			end = new Vector2DInt(Math.max(end.x, edge.start.x), Math.max(end.y, edge.start.y));
			end = new Vector2DInt(Math.max(end.x, edge.end.x), Math.max(end.y, edge.end.y));
		}
		return new Tuple2<>(start, end);
	}

	public static List<Edge18> edges(Vector2DInt position)
	{
		List<Edge18> result = new ArrayList<>();
		result.add(new Edge18(position.add(new Vector2DInt(-1, -1)), position.add(new Vector2DInt(1, -1))));
		result.add(new Edge18(position.add(new Vector2DInt(1, -1)), position.add(new Vector2DInt(1, 1))));
		result.add(new Edge18(position.add(new Vector2DInt(1, 1)), position.add(new Vector2DInt(-1, 1))));
		result.add(new Edge18(position.add(new Vector2DInt(-1, 1)), position.add(new Vector2DInt(-1, -1))));
		return result;
	}

	public static Edge18 find(SortedSet<Edge18> edges, Edge18 edge)
	{
		Edge18 result = null;
		for (Edge18 possible: edges)
		{
			if (edge.isAdjacent(possible) && edge.isHorizontal() == possible.isHorizontal())
			{
				result = possible;
			}
		}
		return result;
	}

	public static Edge18 combine(Edge18 edge0, Edge18 edge1)
	{
		return edge0.start.equals(edge1.end) ? new Edge18(edge1.start, edge0.end) : new Edge18(edge0.start, edge1.end);
	}

	public static void addEdge(SortedSet<Edge18> result, Edge18 edge)
	{
		Edge18 inverse = edge.inverse();
		if (result.contains(inverse))
		{
			result.remove(inverse);
		}
		else
		{
			Edge18 combine = find(result, edge);
			if (combine == null)
			{
				result.add(edge);
			}
			else
			{
				result.remove(combine);
				result.add(combine(edge, combine));
			}
		}
	}

	public static void addEdges(SortedSet<Edge18> result, List<Edge18> edges)
	{
		edges.forEach(edge -> addEdge(result, edge));
	}

	public static void addEdges(SortedSet<Edge18> result, Vector2DInt position)
	{
		addEdges(result, edges(position));
	}

	public SortedSet<Edge18> edgesPart1()
	{
		SortedSet<Edge18> result = new TreeSet<>();
		Vector2DInt position = new Vector2DInt(0, 0);
		for (Dig dig: this.digs)
		{
			for (int index = 0; index < dig.distance; index++)
			{
				position = position.add(dig.direction.multiply(2));
				addEdges(result, position);
			}
		}
		return result;
	}

	public SortedSet<Edge18> edgesPart2()
	{
		SortedSet<Edge18> result = new TreeSet<>();
		Vector2DInt position = new Vector2DInt(0, 0);
		for (Dig dig: this.digs)
		{
			long distance =
				new BigInteger
					(
						String.format
							(
								"%c%c%c%c%c",
								dig.color.get(0),
								dig.color.get(1),
								dig.color.get(2),
								dig.color.get(3),
								dig.color.get(4)
							),
						16
					).longValue();
			SortedSet<Edge18> edges = new TreeSet<>();
			for (long index = 0; index < distance; index++)
			{
				position = position.add(dig.directionPart2().multiply(2));
				addEdges(edges, position);
			}
			edges.forEach(edge -> addEdge(result, edge));
		}
		return result;
	}

	public static SortedSet<Edge18> crossings(SortedSet<Edge18> edges, Vector2DInt position)
	{
		SortedSet<Edge18> result = new TreeSet<>(Comparator.comparingLong(edge0 -> edge0.start.y));
		for (Edge18 edge: edges)
		{
			if (
				(edge.isHorizontal()) &&
					(position.y < edge.start.y) &&
					((edge.start.x < position.x && position.x < edge.end.x)
						|| (edge.end.x < position.x && position.x < edge.start.x))
			)
			{
				result.add(edge);
			}
		}
		return result;
	}

	public static SortedSet<Edge18> path(SortedSet<Edge18> edges, Edge18 edge)
	{
		SortedSet<Edge18> result = new TreeSet<>();
		while (edge != null)
		{
			result.add(edge);
			Edge18 next = null;
			for (Edge18 possible: edges)
			{
				if (!result.contains(possible) && edge.isAdjacent(possible))
				{
					next = possible;
				}
			}
			edge = next;
		}
		return result;
	}

	public static boolean inside(SortedSet<Edge18> edges, Vector2DInt position)
	{
		return crossings(edges, position).size() % 2 == 1;
	}

	public static void print(SortedSet<Edge18> edges)
	{
		Tuple2<Vector2DInt, Vector2DInt> bbox = bbox(edges);
		for (int y = bbox.value0.y + 1; y <= bbox.value1.y - 1; y += 2)
		{
			for (int x = bbox.value0.x + 1; x <= bbox.value1.x - 1; x += 2)
			{
				System.out.print(inside(edges, new Vector2DInt(x, y)) ? '#' : '.');
			}
			System.out.println();
		}
	}

	public static SortedSet<Edge18> outside(final SortedSet<Edge18> edges)
	{
		return path(edges, edges.first());
	}

	public static long inside(SortedSet<Edge18> edges)
	{
		Tuple2<Vector2DInt, Vector2DInt> bbox = bbox(edges);
		long result = 0;
		for (int x = bbox.value0.x + 1; x < bbox.value1.x; x += 2)
		{
			SortedSet<Edge18> crossings = crossings(edges, new Vector2DInt(x, bbox.value0.y - 1));
			while (!crossings.isEmpty())
			{
				long from = crossings.first().start.y;
				crossings.remove(crossings.first());
				long to = crossings.first().start.y;
				crossings.remove(crossings.first());
				result += (to - from) / 2;
			}
		}
		return result;
	}

	public long part1()
	{
		SortedSet<Edge18> edges = outside(edgesPart1());
		return inside(edges);
	}

	public long part2()
	{
		SortedSet<Edge18> edges = outside(edgesPart2());
		return inside(edges);
	}

	public static void main(String[] args)
	{
		Opgave18 opgave = new Opgave18(System.in);
		System.out.println(opgave.part1());
		System.out.println(opgave.part2());
	}
}