package adventofcode;

import java.util.function.Supplier;

public class Grid<Type>
{
	public static <Type> Type[][] create(int sizeX, int sizeY, Supplier<Type> supplier)
	{
		Type[][] result = (Type[][]) new Object[sizeX][sizeY];
		for (int x = 0; x < sizeX; x++)
		{
			for (int y = 0; y < sizeY; y++)
			{
				result[x][y] = supplier.get();
			}
		}
		return result;
	}

	public final Type[][] values;

	public Grid(Type[][] values)
	{
		this.values = values;
	}

	public Grid(int sizeX, int sizeY, Supplier<Type> supplier)
	{
		this(create(sizeX, sizeY, supplier));
	}

	public Grid(int sizeX, int sizeY)
	{
		this(sizeX, sizeY, () -> null);
	}

	public Type get(Vector2DInt vector)
	{
		return this.values[vector.x][vector.y];
	}

	public void set(Vector2DInt vector, Type value)
	{
		this.values[vector.x][vector.y] = value;
	}
}