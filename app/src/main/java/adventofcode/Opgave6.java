package adventofcode;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

class Race
{
	public final long time;
	public final long distance;

	public Race(long time, long distance)
	{
		this.time = time;
		this.distance = distance;
	}

	public long wins()
	{
		long result = 0;
		for (long hold = 0; hold <= this.time; hold++)
		{
			if (this.distance < hold * (this.time - hold))
			{
				result++;
			}
		}
		return result;
	}
}

public class Opgave6
	extends Opgave
{
	public Opgave6(InputStream in)
	{
		super(in);
	}

	public static List<Long> split(String line)
	{
		List<Long> result = new ArrayList<>();
		String numbers = line.split(":\\s+")[1];
		String[] split = numbers.split("\\s+");
		for (int index = 0; index < split.length; index++)
		{
			result.add(Long.parseLong(split[index]));
		}
		return result;
	}

	public long part1()
	{
		List<Race> races = new ArrayList<>();
		for (int index = 0; index < split(lines.get(0)).size(); index++)
		{
			long time = split(lines.get(0)).get(index);
			long distance = split(lines.get(1)).get(index);
			races.add(new Race(time, distance));
		}
		return races.stream().map(race -> race.wins()).reduce(1L, (x, y) -> x * y);
	}

	public static void main(String[] args)
	{
		System.out.println(new Opgave6(System.in).part1());
	}
}
