package adventofcode;

import nl.novit.util.Algorithm;
import nl.novit.util.Tuple2;
import nl.novit.util.Tuple2Comparable;

import java.io.InputStream;
import java.util.*;
import java.util.stream.Collectors;

class Node
	implements Comparable<Node>
{
	public final String id;
	public final String left;
	public final String right;

	public Node(String id, String left, String right)
	{
		this.id = id;
		this.left = left;
		this.right = right;
	}

	@Override
	public int compareTo(Node that)
	{
		return this.id.compareTo(that.id);
	}
}

class Cycle
{
	public final long stepsInitial;
	public final List<Long> stepsPeriodic;

	public Cycle
		(
			long stepsInitial,
			List<Long> stepsPeriodic
		)
	{
		this.stepsInitial = stepsInitial;
		this.stepsPeriodic = stepsPeriodic;
	}
}

public class Opgave8
	extends Opgave
{
	public static Node node(String line)
	{
		String[] splitEquals = line.split("\\s+=\\s+");
		String id = splitEquals[0];
		String[] splitComma = splitEquals[1].split(",\\s+");
		String left = splitComma[0].split("\\(")[1];
		String right = splitComma[1].split("\\)")[0];
		return new Node(id, left, right);
	}

	public static Map<String, Node> nodes(List<String> lines)
	{
		Map<String, Node> result = new TreeMap<>();
		for (String line: lines)
		{
			Node node = node(line);
			result.put(node.id, node);
		}
		return result;
	}

	public final char[] directions;
	public final Map<String, Node> nodes;

	public Opgave8(InputStream in)
	{
		super(in);
		this.directions = lines.get(0).toCharArray();
		this.lines.remove(0);
		this.lines.remove(0);
		this.nodes = nodes(this.lines);
	}

	public Node next(Node node, char direction)
	{
		return this.nodes.get(direction == 'L' ? node.left : node.right);
	}

	public int index(long step)
	{
		return (int) (step % this.directions.length);
	}

	public Tuple2<Node, Long> next(Tuple2<Node, Long> nodeStep)
	{
		return new Tuple2<>(next(nodeStep.value0, this.directions[index(nodeStep.value1)]), nodeStep.value1 + 1);
	}

	public Tuple2<Node, Long> steps(Tuple2<Node, Long> result)
	{
		while (!result.value0.id.endsWith("Z"))
		{
			result = next(result);
		}
		return result;
	}

	Tuple2Comparable<Node, Integer> convert(Tuple2<Node, Long> nodeStep)
	{
		return new Tuple2Comparable<>(nodeStep.value0, index(nodeStep.value1));
	}

	public Cycle cycle(Tuple2<Node, Long> nodeStep)
	{
		nodeStep = steps(nodeStep);
		Long stepsInitial = nodeStep.value1;
		Set<Tuple2Comparable<Node, Integer>> visited = new TreeSet<>();
		List<Long> stepsPeriodic = new ArrayList<>();
		while (visited.add(convert(nodeStep)))
		{
			nodeStep = steps(next(nodeStep));
			stepsPeriodic.add(nodeStep.value1 - (stepsPeriodic.isEmpty() ? stepsInitial : stepsPeriodic.get(stepsPeriodic.size() - 1)));
		}
		return new Cycle
			(
				stepsInitial,
				stepsPeriodic
			);
	}

	public long part1()
	{
		return steps(new Tuple2<>(this.nodes.get("AAA"), 0L)).value1;
	}

	public long part2()
	{
		List<Long> cycles = this.nodes.values()
			.stream()
			.filter(node -> node.id.endsWith("A"))
			.map(node -> cycle(new Tuple2<>(node, 0L)).stepsInitial)
			.collect(Collectors.toList());
		long gcd = cycles
			.stream()
			.reduce(cycles.get(0), Algorithm::gcd);
		return cycles
			.stream()
			.reduce(1L, (x, y) -> x * y / gcd) * gcd;
	}

	public static void main(String[] args)
	{
		Opgave8 opgave8 = new Opgave8(System.in);
		System.out.println(opgave8.part1());
		System.out.println(opgave8.part2());
	}
}