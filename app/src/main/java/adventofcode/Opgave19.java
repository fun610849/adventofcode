package adventofcode;

import nl.novit.util.Tuple2;

import java.io.InputStream;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Opgave19
	extends Opgave
{
	class Range
	{
		public final int from;
		public final int to;

		public Range(int from, int to)
		{
			this.from = from;
			this.to = to;
		}

		public long size()
		{
			return Math.max(0, this.to - this.from);
		}
	}

	class Virtual
	{
		public final Range[] ranges;

		public Virtual(Range[] ranges)
		{
			this.ranges = ranges;
		}

		public long combinations()
		{
			return Arrays.stream(this.ranges)
				.map(Range::size)
				.reduce(1L, (x, y) -> x * y);
		}
	}

	class Part
	{
		public final int[] values;

		public Part(int x, int m, int a, int s)
		{
			this.values = new int[]{x, m, a, s};
		}
	}

	abstract class Condition
	{
		public final int category;
		public final int value;

		public Condition(int category, int value)
		{
			this.category = category;
			this.value = value;
		}

		public abstract boolean match(Part part);

		public abstract Tuple2<Virtual, Virtual> trueFalse(Virtual virtual);
	}

	class ConditionLess
		extends Condition
	{
		public ConditionLess(int category, int value)
		{
			super(category, value);
		}

		@Override
		public boolean match(Part part)
		{
			return part.values[this.category] < this.value;
		}

		@Override
		public Tuple2<Virtual, Virtual> trueFalse(Virtual virtual)
		{
			final Range[] rangesTrue = new Range[virtual.ranges.length];
			final Range[] rangesFalse = new Range[virtual.ranges.length];
			for (int index = 0; index < virtual.ranges.length; index++)
			{
				if (index == this.category)
				{
					rangesTrue[index] = new Range(virtual.ranges[index].from, this.value);
					rangesFalse[index] = new Range(this.value, virtual.ranges[index].to);
				}
				else
				{
					rangesTrue[index] = virtual.ranges[index];
					rangesFalse[index] = virtual.ranges[index];
				}
			}
			return new Tuple2<>
				(
					new Virtual(rangesTrue),
					new Virtual(rangesFalse)
				);
		}

		@Override
		public String toString()
		{
			return this.category + "<" + this.value;
		}
	}

	class ConditionMore
		extends Condition
	{
		public ConditionMore(int category, int value)
		{
			super(category, value);
		}

		@Override
		public boolean match(Part part)
		{
			return part.values[this.category] > this.value;
		}

		@Override
		public Tuple2<Virtual, Virtual> trueFalse(Virtual virtual)
		{
			final Range[] rangesTrue = new Range[virtual.ranges.length];
			final Range[] rangesFalse = new Range[virtual.ranges.length];
			for (int index = 0; index < virtual.ranges.length; index++)
			{
				if (index == this.category)
				{
					rangesTrue[index] = new Range(this.value + 1, virtual.ranges[index].to);
					rangesFalse[index] = new Range(virtual.ranges[index].from, this.value + 1);
				}
				else
				{
					rangesTrue[index] = virtual.ranges[index];
					rangesFalse[index] = virtual.ranges[index];
				}
			}
			return new Tuple2<>
				(
					new Virtual(rangesTrue),
					new Virtual(rangesFalse)
				);
		}

		@Override
		public String toString()
		{
			return this.category + ">" + this.value;
		}
	}

	abstract class Instruction
	{
		public abstract Long execute(Part part);

		public abstract Long execute(Virtual virtual);
	}

	class InstructionAccept
		extends Instruction
	{
		@Override
		public Long execute(Part part)
		{
			return (long) part.values[0] + part.values[1] + part.values[2] + part.values[3];
		}

		@Override
		public Long execute(Virtual virtual)
		{
			return virtual.combinations();
		}

		@Override
		public String toString()
		{
			return "ACCEPT";
		}
	}

	class InstructionReject
		extends Instruction
	{
		@Override
		public Long execute(Part part)
		{
			return 0L;
		}

		@Override
		public Long execute(Virtual virtual)
		{
			return 0L;
		}

		@Override
		public String toString()
		{
			return "REJECT";
		}
	}

	class InstructionMove
		extends Instruction
	{
		public final String chain;

		public InstructionMove(String chain)
		{
			this.chain = chain;
		}

		@Override
		public Long execute(Part part)
		{
			return Opgave19.this.chains.get(this.chain).execute(part);
		}

		@Override
		public Long execute(Virtual virtual)
		{
			return Opgave19.this.chains.get(this.chain).execute(virtual);
		}

		@Override
		public String toString()
		{
			return "MOVE(" + this.chain + ")";
		}
	}

	class Chain
	{
		public final String name;
		public final List<Tuple2<Condition, Instruction>> conditionInstructions;
		public final Instruction instructionDefault;

		public Chain(String name, List<Tuple2<Condition, Instruction>> conditionInstructions, Instruction instructionDefault)
		{
			this.name = name;
			this.conditionInstructions = conditionInstructions;
			this.instructionDefault = instructionDefault;
		}

		public Long execute(Part part)
		{
			Long result = null;
			for (int index = 0; result == null && index < this.conditionInstructions.size(); index++)
			{
				Tuple2<Condition, Instruction> conditionInstruction = this.conditionInstructions.get(index);
				if (conditionInstruction.value0.match(part))
				{
					result = conditionInstruction.value1.execute(part);
				}
			}
			if (result == null)
			{
				result = instructionDefault.execute(part);
			}
			return result;
		}

		public long execute(Virtual virtual)
		{
			long result = 0;
			for (int index = 0; index < this.conditionInstructions.size(); index++)
			{
				Tuple2<Condition, Instruction> conditionInstruction = this.conditionInstructions.get(index);
				Tuple2<Virtual, Virtual> trueFalse = conditionInstruction.value0.trueFalse(virtual);
				result += conditionInstruction.value1.execute(trueFalse.value0);
				virtual = trueFalse.value1;
			}
			result += instructionDefault.execute(virtual);
			return result;
		}
	}

	public static final Map<Character, Integer> CATEGORIES =
		Map.of
			(
				'x', 0,
				'm', 1,
				'a', 2,
				's', 3
			);

	public Instruction instruction(String instruction)
	{
		return instruction.contains("A") ? new InstructionAccept() : instruction.contains("R") ? new InstructionReject() : new InstructionMove(instruction);
	}

	public final Map<String, Chain> chains;
	public List<Part> parts;

	public Opgave19(InputStream in)
	{
		super(in);
		this.chains = new TreeMap<>();
		this.parts = new ArrayList<>();
		Pattern patternChain = Pattern.compile("([a-z]+)\\{(.*)\\}");
		Pattern patternPart = Pattern.compile("\\{x=(\\d*),m=(\\d*),a=(\\d*),s=(\\d*)\\}");
		for (String line: lines)
		{
			Matcher matcherChain = patternChain.matcher(line);
			if (matcherChain.matches())
			{
				String name = matcherChain.group(1);
				List<Tuple2<Condition, Instruction>> conditionInstructions = new ArrayList<>();
				String[] split = matcherChain.group(2).split(",");
				for (int index = 0; index < split.length - 1; index++)
				{
					String conditionInstruction = split[index];
					String condition = conditionInstruction.split(":")[0];
					int category = CATEGORIES.get(condition.charAt(0));
					int value = Integer.parseInt(condition.split("[<>]")[1]);
					conditionInstructions.add
						(
							new Tuple2<>
								(
									conditionInstruction.contains("<") ? new ConditionLess(category, value) : new ConditionMore(category, value),
									instruction(conditionInstruction.split(":")[1])
								)
						);
				}
				Chain chain = new Chain
					(
						name,
						conditionInstructions,
						instruction(split[split.length - 1])
					);
				this.chains.put(chain.name, chain);
			}
			Matcher matcherPart = patternPart.matcher(line);
			if (matcherPart.matches())
			{
				this.parts.add
					(
						new Part
							(
								Integer.parseInt(matcherPart.group(1)),
								Integer.parseInt(matcherPart.group(2)),
								Integer.parseInt(matcherPart.group(3)),
								Integer.parseInt(matcherPart.group(4))
							)
					);
			}
		}
	}

	public long part1()
	{
		long result = 0;
		for (Part part: this.parts)
		{
			result += this.chains.get("in").execute(part);
		}
		return result;
	}

	public long part2()
	{
		Range range = new Range(1, 4001);
		return this.chains.get("in").execute(new Virtual(new Range[]{range, range, range, range}));
	}

	public static void main(String[] args)
	{
		Opgave19 opgave = new Opgave19(System.in);
		System.out.println(opgave.part1());
		System.out.println(opgave.part2());
	}
}