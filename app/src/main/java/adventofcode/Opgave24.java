package adventofcode;

import nl.novit.util.Tuple2;

import java.io.InputStream;
import java.util.List;
import java.util.stream.Collectors;

class Path
{
	final Vector3DFraction position;
	final Vector3DFraction speed;

	public Path(Vector3DFraction position, Vector3DFraction speed)
	{
		this.position = position;
		this.speed = speed;
	}

	public Path(String input)
	{
		this(new Vector3DFraction(input.split("\\s*@\\s* ")[0]), new Vector3DFraction(input.split("\\s*@\\s*")[1]));
	}

	public Tuple2<Fraction, Fraction> times2D(Path that)
	{
		Path p = this;
		Path q = that;
		Fraction psx = this.position.x;
		Fraction psy = this.position.y;
		Fraction pvx = this.speed.x;
		Fraction pvy = this.speed.y;
		Fraction qsx = that.position.x;
		Fraction qsy = that.position.y;
		Fraction qvx = that.speed.x;
		Fraction qvy = that.speed.y;
		Fraction numeratorThis = qsx.negate().multiply(qvy).add(psx.multiply(qvy).add(qsy.subtract(psy).multiply(qvx)));
		Fraction numeratorThat = pvx.multiply(qsy.subtract(psy)).subtract(pvy.multiply(qsx)).add(psx.multiply(pvy));
		Fraction denominator = pvy.multiply(qvx).subtract(pvx.multiply(qvy));
		return denominator.equals(Fraction.ZERO) ? null : new Tuple2<>(numeratorThis.divide(denominator), numeratorThat.divide(denominator));
	}

	public Vector2DFraction collision(Path that)
	{
		Vector2DFraction result;
		Tuple2<Fraction, Fraction> times = times2D(that);
		if (times == null)
		{
			result = null;
		}
		else
		{
			if (Fraction.ZERO.compareTo(times.value0) < 0 && Fraction.ZERO.compareTo(times.value1) < 0)
			{
				Vector3DFraction vectorThis = this.position.add(this.speed.multiply(times.value0));
				result = new Vector2DFraction(vectorThis.x, vectorThis.y);
			}
			else
			{
				result = null;
			}
		}
		return result;
	}
}

public class Opgave24
	extends Opgave
{
	public final List<Path> paths;

	public Opgave24(InputStream in)
	{
		super(in);
		this.paths = this.lines.stream().map(Path::new).collect(Collectors.toList());
	}

	public long part1()
	{
		long start = 200000000000000L;
		long end = 400000000000000L;
		Vector2DFraction testStart = new Vector2DFraction(start, start);
		Vector2DFraction testEnd = new Vector2DFraction(end, end).add(testStart.multiply(Fraction.ONE.negate()));
		long result = 0;
		for (Path path0: this.paths)
		{
			for (Path path1: this.paths)
			{
				if (path0 != path1)
				{
					Vector2DFraction collision = path0.collision(path1);
					if (collision != null)
					{
						collision = collision.add(testStart.multiply(Fraction.ONE.negate()));
						if (collision.insideBoundingBox(testEnd.x, testEnd.y))
						{
							result += 1;
						}
					}
				}
			}
		}
		return result / 2;
	}

	public long part2()
	{
		long result = 0;
		return result;
	}

	public static void main(String[] args)
	{
		Opgave24 opgave = new Opgave24(System.in);
		System.out.println(opgave.part1());
		System.out.println(opgave.part2());
	}
}