package adventofcode;

import java.util.ArrayList;
import java.util.List;

public class Vector3DInt
	implements Comparable<Vector3DInt>
{
	public static List<Vector3DInt> vectors(int sizeX, int sizeY, int sizeZ)
	{
		List<Vector3DInt> result = new ArrayList<>();
		for (int x = 0; x < sizeX; x++)
		{
			for (int y = 0; y < sizeY; y++)
			{
				for (int z = 0; z < sizeY; z++)
				{
					result.add(new Vector3DInt(x, y, z));
				}
			}
		}
		return result;
	}

	public final int x;
	public final int y;
	public final int z;

	public Vector3DInt(int x, int y, int z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public Vector3DInt(String[] input)
	{
		this(Integer.parseInt(input[0]), Integer.parseInt(input[1]), Integer.parseInt(input[2]));
	}

	public Vector3DInt(String input)
	{
		this(input.split(","));
	}

	@Override
	public int compareTo(Vector3DInt that)
	{
		int result = Long.compare(this.x, that.x);
		if (result == 0)
		{
			result = Long.compare(this.y, that.y);
			if (result == 0)
			{
				result = Long.compare(this.z, that.z);
			}
		}
		return result;
	}

	public boolean equals(Vector3DInt that)
	{
		return compareTo(that) == 0;
	}

	public Vector3DInt add(Vector3DInt vector)
	{
		return new Vector3DInt(this.x + vector.x, this.y + vector.y, this.z + vector.z);
	}

	public boolean insideBoundingBox(long sizeX, long sizeY, long sizeZ)
	{
		return 0 <= this.x && this.x < sizeX && 0 <= this.y && this.y < sizeY && 0 <= this.z && this.z < sizeZ;
	}

	public Vector3DInt multiply(int factor)
	{
		return new Vector3DInt(factor * this.x, factor * this.y, factor * this.z);
	}

	@Override
	public String toString()
	{
		return "(" + this.x + ", " + this.y + ", " + this.z + ")";
	}
}