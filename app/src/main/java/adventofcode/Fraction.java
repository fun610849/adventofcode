package adventofcode;

import java.math.BigInteger;

public class Fraction
	implements Comparable<Fraction>
{
	public static final Fraction ZERO = new Fraction(0);
	public static final Fraction ONE = new Fraction(1);

	public static BigInteger gcd(BigInteger a, BigInteger b)
	{
		return BigInteger.ZERO.equals(b) ? a : gcd(b, a.mod(b));
	}

	public final BigInteger nominator;
	public final BigInteger denominator;

	public Fraction(BigInteger integer)
	{
		this(integer, BigInteger.ONE);
	}

	public Fraction(BigInteger nominator, BigInteger denominator)
	{
		if (denominator.equals(BigInteger.ZERO))
		{
			throw new ArithmeticException();
		}
		if (denominator.compareTo(BigInteger.ZERO) < 0)
		{
			nominator = nominator.negate();
			denominator = denominator.negate();
		}
		BigInteger gcd = gcd(nominator, denominator);
		this.nominator = nominator.divide(gcd);
		this.denominator = denominator.divide(gcd);
	}

	public Fraction(long nominator, long denominator)
	{
		this(BigInteger.valueOf(nominator), BigInteger.valueOf(denominator));
	}

	public Fraction(long integer)
	{
		this(integer, 1);
	}

	@Override
	public int compareTo(Fraction that)
	{
		return this.nominator.multiply(that.denominator).compareTo(that.nominator.multiply(this.denominator));
	}

	public boolean equals(Fraction that)
	{
		return compareTo(that) == 0;
	}

	public Fraction add(Fraction fraction)
	{
		return new Fraction(this.nominator.multiply(fraction.denominator).add(this.denominator.multiply(fraction.nominator)), this.denominator.multiply(fraction.denominator));
	}

	public Fraction multiply(Fraction fraction)
	{
		return new Fraction(this.nominator.multiply(fraction.nominator), this.denominator.multiply(fraction.denominator));
	}

	public Fraction negate()
	{
		return new Fraction(this.nominator.negate(), this.denominator);
	}

	public Fraction subtract(Fraction fraction)
	{
		return add(fraction.negate());
	}

	public Fraction divide(Fraction fraction)
	{
		return multiply(new Fraction(fraction.denominator, fraction.nominator));
	}

	@Override
	public String toString()
	{
		BigInteger nominator = this.nominator;
		String result = "(";
		if (nominator.compareTo(BigInteger.ZERO) < 0)
		{
			result += "-";
			nominator = nominator.negate();
		}
		if (nominator.mod(this.denominator).equals(BigInteger.ZERO))
		{
			result += nominator.divide(this.denominator);
		}
		else
		{
			if (this.denominator.compareTo(nominator) <= 0)
			{
				result += nominator.divide(this.denominator) + " ";
			}
			result += nominator.mod(this.denominator) + "/" + this.denominator;
		}
		result += ")";
		return result;
	}
}