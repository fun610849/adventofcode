package adventofcode;

public class Vector3DFraction
	implements Comparable<Vector3DFraction>
{
	public final Fraction x;
	public final Fraction y;
	public final Fraction z;

	public Vector3DFraction(Fraction x, Fraction y, Fraction z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public Vector3DFraction(long x, long y, long z)
	{
		this(new Fraction(x), new Fraction(y), new Fraction(z));
	}

	public Vector3DFraction(Vector3DInt vector)
	{
		this(vector.x, vector.y, vector.z);
	}

	public Vector3DFraction(String[] input)
	{
		this(Long.parseLong(input[0]), Long.parseLong(input[1]), Long.parseLong(input[2]));
	}

	public Vector3DFraction(String input)
	{
		this(input.split(",\\s*"));
	}

	@Override
	public int compareTo(Vector3DFraction that)
	{
		int result = this.x.compareTo(that.x);
		if (result == 0)
		{
			result = this.y.compareTo(that.y);
			if (result == 0)
			{
				result = this.z.compareTo(that.z);
			}
		}
		return result;
	}

	public boolean equals(Vector3DFraction that)
	{
		return compareTo(that) == 0;
	}

	public Vector3DFraction add(Vector3DFraction vector)
	{
		return new Vector3DFraction(this.x.add(vector.x), this.y.add(vector.y), this.z.add(vector.z));
	}

	public boolean insideBoundingBox(Fraction sizeX, Fraction sizeY, Fraction sizeZ)
	{
		return Fraction.ZERO.compareTo(this.x) <= 0 && this.x.compareTo(sizeX) <= 0 && Fraction.ZERO.compareTo(this.y) <= 0 && this.y.compareTo(sizeY) <= 0 && Fraction.ZERO.compareTo(this.z) <= 0 && this.z.compareTo(sizeZ) <= 0;
	}

	public Vector3DFraction multiply(Fraction factor)
	{
		return new Vector3DFraction(this.x.multiply(factor), this.y.multiply(factor), this.z.multiply(factor));
	}

	@Override
	public String toString()
	{
		return "(" + this.x + ", " + this.y + ", " + this.z + ")";
	}
}