package adventofcode;

import nl.novit.parser.exception.ExceptionParserInput;
import nl.novit.parser.parser.ParserLL1;
import nl.novit.parser.rule.Rule;
import nl.novit.parser.util.StringParser;
import nl.novit.parser.util.StringParserDefault;
import nl.novit.util.Util;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

enum Tri
{
	UNKNOWN, TRUE, FALSE
}

class Record
{
	public static <Type> List<Type> times5(List<Type> list, Type separator)
	{
		List<Type> result = new ArrayList<>();
		for (int index = 0; index < 5; index++)
		{
			result.addAll(list);
			if (separator != null)
			{
				result.add(separator);
			}
		}
		if (separator != null)
		{
			result.remove(result.size() - 1);
		}
		return result;
	}

	public final List<Tri> springs;
	public final List<Integer> sizes;

	public Record(List<Tri> springs, List<Integer> sizes)
	{
		this.springs = springs;
		this.sizes = sizes;
	}

	public boolean check(boolean[] springs)
	{
		boolean result = true;
		for (int index = 0; index < springs.length; index++)
		{
			Tri tri = this.springs.get(index);
			result &= (tri != Tri.FALSE) || !springs[index];
			result &= (tri != Tri.TRUE) || springs[index];
		}
		return result;
	}

	public List<Integer> groups(boolean[] springs)
	{
		List<Integer> result = new ArrayList<>();
		Integer length = null;
		for (boolean spring: springs)
		{
			if (length == null)
			{
				if (spring)
				{
					length = 1;
				}
			}
			else
			{
				if (spring)
				{
					length += 1;
				}
				else
				{
					result.add(length);
					length = null;
				}
			}
		}
		if (length != null)
		{
			result.add(length);
		}
		return result;
	}

	public boolean[] springs(int size, int number)
	{
		boolean[] result = new boolean[size];
		for (int index = 0; index < size; index++)
		{
			result[index] = number % 2 == 1;
			number /= 2;
		}
		return result;
	}

	public long brute()
	{
		long result = 0;
		int size = this.springs.size();
		for (int number = 0; number < (1 << size); number++)
		{
			boolean[] springs = springs(size, number);
			if (check(springs) && Util.compareIterable(this.sizes, groups(springs)) == 0)
			{
				result += 1;
			}
		}
		return result;
	}

	public boolean check(int positionStart, int positionEnd, Tri tri)
	{
		boolean result = true;
		for (int position = positionStart; position < positionEnd; position++)
		{
			result &= this.springs.get(position) != tri;
		}
		return result;
	}

	public boolean checkEmpty(int positionStart, int positionEnd)
	{
		return check(positionStart, positionEnd, Tri.TRUE);
	}

	public boolean checkFull(int positionStart, int positionEnd)
	{
		return positionEnd <= this.springs.size() && check(positionStart, positionEnd, Tri.FALSE);
	}

	public long smart()
	{
		long[] position2Possibilities = new long[this.springs.size()];
		for (int indexCurrent = 0; indexCurrent < this.sizes.size(); indexCurrent++)
		{
			long[] position2PossibilitiesNew = new long[this.springs.size()];
			for (int positionCurrent = 0; positionCurrent < this.springs.size(); positionCurrent++)
			{
				position2PossibilitiesNew[positionCurrent] = 0;
				if (indexCurrent == 0)
				{
					if (checkEmpty(0, positionCurrent) && checkFull(positionCurrent, positionCurrent + this.sizes.get(indexCurrent)))
					{
						position2PossibilitiesNew[positionCurrent] += 1;
					}
				}
				else
				{
					for (int positionLast = 0; positionLast + this.sizes.get(indexCurrent - 1) < positionCurrent; positionLast++)
					{
						if (checkEmpty(positionLast + this.sizes.get(indexCurrent - 1), positionCurrent) && checkFull(positionCurrent, positionCurrent + this.sizes.get(indexCurrent)))
						{
							position2PossibilitiesNew[positionCurrent] += position2Possibilities[positionLast];
						}
					}
				}
			}
			position2Possibilities = position2PossibilitiesNew;
		}
		long result = 0;
		for (int position = 0; position < this.springs.size(); position++)
		{
			if (checkEmpty(position + this.sizes.get(this.sizes.size() - 1), this.springs.size()))
			{
				result += position2Possibilities[position];
			}
		}
		return result;
	}
}

public class Opgave12
	extends Opgave
{
	public final List<Record> records;

	public Opgave12(InputStream in)
		throws ExceptionParserInput
	{
		super(in);
		final StringParser<Record> parser =
			new StringParserDefault<>
				(
					new ParserLL1<>
						(
							new BuilderRuleAdvent<Record>()
							{
								@Override
								public Rule<Character, Record> rule()
								{
									Rule<Character, Tri> ruleTri =
										union
											(
												terminal('.', Tri.FALSE),
												terminal('#', Tri.TRUE),
												terminal('?', Tri.UNKNOWN)
											);
									return
										identity2
											(
												Record::new,
												concatenation11
													(
														repetition(ruleTri),
														concatenation01
															(
																dummy(terminal(' ')),
																repetition(this.ruleInteger, dummy(terminal(',')))
															)
													)
											);
								}
							}.rule()));
		this.records = new ArrayList<>();
		for (String line: this.lines)
		{
			this.records.add(parser.parse(line).getTarget());
		}
	}

	public long part1()
	{
		return this.records
			.stream()
			.map(Record::brute)
			.reduce(0L, Long::sum);
	}

	public long part2()
	{
		final List<Record> records5Times =
			this.records
				.stream()
				.map
					(
						record ->
							new Record
								(
									Record.times5(record.springs, Tri.UNKNOWN),
									Record.times5(record.sizes, null)
								)
					)
				.collect(Collectors.toList());
		return records5Times
			.stream()
			.map(Record::smart)
			.reduce(0L, Long::sum);
	}

	public static void main(String[] args)
		throws ExceptionParserInput
	{
		Opgave12 opgave = new Opgave12(System.in);
		System.out.println(opgave.part1());
		System.out.println(opgave.part2());
	}
}