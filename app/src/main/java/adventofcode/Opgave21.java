package adventofcode;

import nl.novit.util.Tuple2Comparable;

import java.io.InputStream;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public class Opgave21
	extends Opgave
{
	public final int sizeX;
	public final int sizeY;
	public final int sizeX2;
	public final int sizeY2;
	public final Grid<Boolean> rocks;
	public Vector2DInt start;
	public final Grid<Boolean> rocks2;
	public final Grid<Grid<Integer>> distance2;
	public final long reachableAlways2Odd;
	public final long reachableAlways2Even;

	public Opgave21(InputStream in)
	{
		super(in);
		this.sizeX = this.lines.get(0).length();
		this.sizeY = this.lines.size();
		this.rocks = new Grid<>(this.sizeX, this.sizeY);
		this.start = null;
		for (Vector2DInt vector: Vector2DInt.vectors(this.sizeX, this.sizeY))
		{
			char character = this.lines.get(vector.y).charAt(vector.x);
			this.rocks.set(vector, character == '#');
			if (character == 'S')
			{
				this.start = vector;
			}
		}
		this.sizeX2 = 2 * this.sizeX;
		this.sizeY2 = 2 * this.sizeY;
		this.rocks2 = new Grid<>(this.sizeX2, this.sizeY2);
		for (Vector2DInt vector: Vector2DInt.vectors(this.sizeX2, this.sizeY2))
		{
			this.rocks2.set(vector, this.rocks.get(mod(vector.add(this.start), this.sizeX)));
		}
		this.distance2 = new Grid<>(this.sizeX2, this.sizeY2);
		for (Vector2DInt start: Vector2DInt.vectors(this.sizeX2, this.sizeY2))
		{
			if (start.x == 0 || start.x == this.sizeX2 - 1 || start.y == 0 || start.y == this.sizeY2 - 1)
			{
				if (!this.rocks2.get(start))
				{
					Grid<Integer> grid = new Grid<>(this.sizeX2, this.sizeY2);
					this.distance2.set(start, grid);
					grid.set(start, 0);
					Set<Vector2DInt> changed = new TreeSet<>();
					changed.add(start);
					while (!changed.isEmpty())
					{
						Set<Vector2DInt> next = new TreeSet<>();
						for (Vector2DInt vector: changed)
						{
							for (Vector2DInt direction: Vector2DInt.DIRECTIONS4)
							{
								Vector2DInt neighbour = vector.add(direction);
								if (neighbour.insideBoundingBox(this.sizeX2, this.sizeY2) && !this.rocks2.get(neighbour) && grid.get(neighbour) == null)
								{
									grid.set(neighbour, grid.get(vector) + 1);
									next.add(neighbour);
								}
							}
						}
						changed = next;
					}
				}
			}
		}
		this.reachableAlways2Odd = reachable(new Vector2DInt(0, 0), this.sizeX2 + this.sizeY2, 1).size();
		this.reachableAlways2Even = reachable(new Vector2DInt(0, 0), this.sizeX2 + this.sizeY2, 0).size();
	}

	public Vector2DInt mod(Vector2DInt vector, int size)
	{
		return new Vector2DInt((size + vector.x % size) % size, (size + vector.y % size) % size);
	}

	public Set<Vector2DInt> reachableFrom(Set<Vector2DInt> current, Set<Vector2DInt> last, boolean infinite)
	{
		Set<Vector2DInt> result = new TreeSet<>();
		for (Vector2DInt vector: current)
		{
			for (Vector2DInt direction: Vector2DInt.DIRECTIONS4)
			{
				Vector2DInt next = vector.add(direction);
				if (!last.contains(next))
				{
					if (infinite || next.insideBoundingBox(this.sizeX, this.sizeY))
					{
						if (!this.rocks.get(mod(next, this.sizeX)))
						{
							result.add(next);
						}
					}
				}
			}
		}
		return result;
	}

	public long solve(int steps, boolean infinite)
	{
		Set<Vector2DInt> current = new TreeSet<>();
		current.add(this.start);
		Set<Vector2DInt> last = new TreeSet<>();
		long result = 0;
		for (int step = 0; step < steps; step++)
		{
			if (step % 2 == steps % 2)
			{
				result += current.size();
			}
			Set<Vector2DInt> next = reachableFrom(current, last, infinite);
			last = current;
			current = next;
		}
		result += current.size();
		return result;
	}

	public long part1()
	{
		return solve(64, false);
	}

	public Set<Vector2DInt> reachable(Vector2DInt start, int steps, int parity)
	{
		Set<Vector2DInt> result = new TreeSet<>();
		Grid<Integer> distance = this.distance2.get(start);
		if (distance != null)
		{
			for (Vector2DInt vector: Vector2DInt.vectors(this.sizeX2, this.sizeY2))
			{
				if ((vector.x + vector.y) % 2 == parity)
				{
					Integer step = distance.get(vector);
					if (step != null && step <= steps)
					{
						result.add(vector);
					}
				}
			}
		}
		return result;
	}

	public long reachableMax(Vector2DInt start, int steps, int parity)
	{
		Set<Vector2DInt> result = new TreeSet<>();
		if (start.x == 0)
		{
			if (start.y == 0)
			{
				result = reachable(start, steps, parity);
			}
			else
			{
				for (int x = 0; x < this.sizeX2; x++)
				{
					result.addAll(reachable(new Vector2DInt(start.x + x, start.y - 1), steps - 1 - x, parity));
				}
			}
		}
		else
		{
			if (start.y == 0)
			{
				for (int y = 0; y < this.sizeY2; y++)
				{
					result.addAll(reachable(new Vector2DInt(start.x - 1, start.y + y), steps - 1 - y, parity));
				}
			}
			else
			{
				for (int x = 1; x <= this.sizeX2; x++)
				{
					result.addAll(reachable(new Vector2DInt(start.x - x, start.y - 1), steps - 1 - x, parity));
				}
				for (int y = 1; y <= this.sizeY2; y++)
				{
					result.addAll(reachable(new Vector2DInt(start.x - 1, start.y - y), steps - 1 - y, parity));
				}
			}
		}
		return result.size();
	}

	public long reachableCached(Vector2DInt start, int steps, int parity, Map<Tuple2Comparable<Vector2DInt, Integer>, Long> cache)
	{
		long result;
		if (steps < 0)
		{
			result = 0;
		}
		else
		{
			if (this.sizeX2 + this.sizeY2 < steps)
			{
				result = parity == 0 ? this.reachableAlways2Even : this.reachableAlways2Odd;
			}
			else
			{
				result = cache.computeIfAbsent(new Tuple2Comparable<>(start, steps), x -> reachableMax(x.value0, x.value1, parity));
			}
		}
		return result;
	}

	public long reachable(Vector2DInt start, int steps, int offsetX, int offsetY, int parity, Map<Tuple2Comparable<Vector2DInt, Integer>, Long> cache)
	{
		int deltaX = offsetX * this.sizeX2;
		int deltaY = offsetY * this.sizeY2;
		start = mod(new Vector2DInt(start.x + deltaX, start.y + deltaY), this.sizeY2);
		if (offsetX < 0)
		{
			start = new Vector2DInt(start.x + this.sizeX2, start.y);
			steps += deltaX + this.sizeX2;
		}
		else
		{
			steps -= deltaX;
		}
		if (offsetY < 0)
		{
			start = new Vector2DInt(start.x, start.y + this.sizeY2);
			steps += deltaY + this.sizeY2;
		}
		else
		{
			steps -= deltaY;
		}
		return reachableCached(start, steps, parity, cache);
	}

	public long solveInfiniteFast(int steps)
	{
		long result = 0;
		int size = steps / this.sizeX2 + 1;
		Map<Tuple2Comparable<Vector2DInt, Integer>, Long> cache = new TreeMap<>();
		for (int offsetX = -size; offsetX <= size; offsetX++)
		{
			for (int offsetY = -size; offsetY <= size; offsetY++)
			{
				result += reachable(new Vector2DInt(0, 0), steps, offsetX, offsetY, steps % 2, cache);
			}
		}
		return result;
	}

	public long part2()
	{
		return solveInfiniteFast(26501365);
	}

	public static void main(String[] args)
	{
		Opgave21 opgave = new Opgave21(System.in);
		System.out.println(opgave.part1());
		System.out.println(opgave.part2());
	}
}