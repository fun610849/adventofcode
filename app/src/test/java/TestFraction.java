import adventofcode.Fraction;
import adventofcode.Tests;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestFraction
	extends Tests
{
	@Test
	public void testCompare()
	{
		Assertions.assertEquals
			(
				0,
				new Fraction(1, -1).compareTo(new Fraction(-1, 1))
			);
		Assertions.assertEquals
			(
				-1,
				new Fraction(1, -1).compareTo(new Fraction(1, 1))
			);
		Assertions.assertEquals
			(
				1,
				new Fraction(1, 1).compareTo(new Fraction(1, -1))
			);
		Assertions.assertEquals
			(
				1,
				new Fraction(-1, 1).compareTo(new Fraction(-2, 1))
			);
		Assertions.assertEquals
			(
				-1,
				new Fraction(2, 3).compareTo(new Fraction(3, 4))
			);
		Assertions.assertEquals
			(
				1,
				new Fraction(-2, 3).compareTo(new Fraction(-3, 4))
			);
	}

	@Test
	public void testToString()
	{
		Assertions.assertEquals("(0)", new Fraction(0).toString());
		Assertions.assertEquals("(1)", new Fraction(1).toString());
		Assertions.assertEquals("(-1)", new Fraction(-1).toString());
		Assertions.assertEquals("(1)", new Fraction(1, 1).toString());
		Assertions.assertEquals("(1/2)", new Fraction(1, 2).toString());
		Assertions.assertEquals("(-1/2)", new Fraction(-1, 2).toString());
		Assertions.assertEquals("(2)", new Fraction(4, 2).toString());
		Assertions.assertEquals("(2 1/2)", new Fraction(5, 2).toString());
		Assertions.assertEquals("(-2 1/2)", new Fraction(-5, 2).toString());
	}
}