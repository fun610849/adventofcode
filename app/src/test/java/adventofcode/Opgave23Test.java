package adventofcode;

import org.junit.jupiter.api.Test;

public class Opgave23Test
	extends Tests
{
	@Test
	public void testPart2()
	{
		new Opgave23(Opgave.open("input23.txt")).part1();
	}
}