package adventofcode;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class Opgave25Test
	extends Tests
{
	public static final Opgave25 OPGAVE = new Opgave25(Opgave.open("input25.txt"));

	@Test
	public void testPart1()
	{
		Assertions.assertEquals(-1, OPGAVE.part1());
	}
}