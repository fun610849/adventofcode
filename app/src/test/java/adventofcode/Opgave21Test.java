package adventofcode;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class Opgave21Test
	extends Tests
{
	public static final Opgave21 EMPTY = new Opgave21(Opgave.open("empty21.txt"));
	public static final Opgave21 SIMPLE = new Opgave21(Opgave.open("simple21.txt"));
	public static final Opgave21 TEST = new Opgave21(Opgave.open("test21.txt"));
	public static final Opgave21 OPGAVE = new Opgave21(Opgave.open("input21.txt"));

	@Test
	public void testSize()
	{
		Assertions.assertEquals(5, EMPTY.sizeX);
		Assertions.assertEquals(5, EMPTY.sizeY);
		Assertions.assertEquals(5, EMPTY.sizeY);
		Assertions.assertEquals(5 / 2, EMPTY.start.x);
		Assertions.assertEquals(5 / 2, EMPTY.start.x);
		Assertions.assertEquals(11, TEST.sizeX);
		Assertions.assertEquals(11, TEST.sizeY);
		Assertions.assertEquals(11, TEST.sizeY);
		Assertions.assertEquals(11 / 2, TEST.start.x);
		Assertions.assertEquals(11 / 2, TEST.start.y);
		Assertions.assertEquals(131, OPGAVE.sizeX);
		Assertions.assertEquals(131, OPGAVE.sizeY);
		Assertions.assertEquals(131 / 2, OPGAVE.start.x);
		Assertions.assertEquals(131 / 2, OPGAVE.start.y);
		for (int i = 0; i < 131; i++)
		{
			Assertions.assertFalse(OPGAVE.rocks.get(new Vector2DInt(i, 131 / 2)));
			Assertions.assertFalse(OPGAVE.rocks.get(new Vector2DInt(131 / 2, i)));
		}
		Assertions.assertEquals(262, OPGAVE.sizeX2);
		Assertions.assertEquals(262, OPGAVE.sizeY2);
		for (int i = 0; i < 262; i++)
		{
			Assertions.assertFalse(OPGAVE.rocks2.get(new Vector2DInt(i, 0)));
			Assertions.assertFalse(OPGAVE.rocks2.get(new Vector2DInt(0, i)));
		}
	}

	@Test
	public void testSolvePart1()
	{
		Assertions.assertEquals(16, TEST.solve(6, false));
		Assertions.assertEquals(3830, OPGAVE.solve(64, false));
	}

	@Test
	public void testSolvePart2Empty()
	{
		Assertions.assertEquals(1, EMPTY.solve(0, true));
		Assertions.assertEquals(1 + 4 * 2, EMPTY.solve(2, true));
		Assertions.assertEquals(1 + 4 * 2 * (1 + 2), EMPTY.solve(4, true));
		Assertions.assertEquals(1 + 4 * 2 * (1 + 2 + 3), EMPTY.solve(6, true));
		Assertions.assertEquals(1 + 4 * 2 * (1 + 2 + 3 + 4), EMPTY.solve(8, true));
		Assertions.assertEquals(1 + 4 * 2 * (1 + 2 + 3 + 4 + 5), EMPTY.solve(10, true));
		Assertions.assertEquals(1 + 4 * 2 * (1 + 2 + 3 + 4 + 5 + 6), EMPTY.solve(12, true));
		Assertions.assertEquals(1 + 4 * 2 * (1 + 2 + 3 + 4 + 5 + 6 + 7), EMPTY.solve(14, true));
		Assertions.assertEquals(1 + 4 * 2 * (1 + 2 + 3 + 4 + 5 + 6 + 7 + 8), EMPTY.solve(16, true));
		Assertions.assertEquals((1 + 1) * (1 + 1), EMPTY.solve(1, true));
		Assertions.assertEquals((3 + 1) * (3 + 1), EMPTY.solve(3, true));
		Assertions.assertEquals((5 + 1) * (5 + 1), EMPTY.solve(5, true));
		Assertions.assertEquals((7 + 1) * (7 + 1), EMPTY.solve(7, true));
		Assertions.assertEquals((9 + 1) * (9 + 1), EMPTY.solve(9, true));
		Assertions.assertEquals((11 + 1) * (11 + 1), EMPTY.solve(11, true));
		Assertions.assertEquals((13 + 1) * (13 + 1), EMPTY.solve(13, true));
		Assertions.assertEquals((15 + 1) * (15 + 1), EMPTY.solve(15, true));
	}

	@Test
	public void testSolvePart2Simple()
	{
		Assertions.assertEquals(1, SIMPLE.solve(0, true));
		Assertions.assertEquals(4, SIMPLE.solve(1, true));
		Assertions.assertEquals(8, SIMPLE.solve(2, true));
		Assertions.assertEquals(14, SIMPLE.solve(3, true));
		Assertions.assertEquals(23, SIMPLE.solve(4, true));
		Assertions.assertEquals(32, SIMPLE.solve(5, true));
		Assertions.assertEquals(43, SIMPLE.solve(6, true));
	}

	@Test
	public void testSolvePart2Test()
	{
		Assertions.assertEquals(2, TEST.solve(1, true));
		Assertions.assertEquals(6, TEST.solve(3, true));
		Assertions.assertEquals(13, TEST.solve(5, true));
		Assertions.assertEquals(16, TEST.solve(6, true));
		Assertions.assertEquals(50, TEST.solve(10, true));
		Assertions.assertEquals(1594, TEST.solve(50, true));
		Assertions.assertEquals(6536, TEST.solve(100, true));
		Assertions.assertEquals(167004, TEST.solve(500, true));
		Assertions.assertEquals(668697, TEST.solve(1000, true));
		Assertions.assertEquals(16733044, TEST.solve(5000, true));
	}

	@Test
	public void testSolvePart2Opgave()
	{
		Assertions.assertEquals(909271, OPGAVE.solve(1000, true));
		Assertions.assertEquals(911429, OPGAVE.solve(1001, true));
		Assertions.assertEquals(22686298, OPGAVE.solve(5000, true));
		Assertions.assertEquals(22695072, OPGAVE.solve(5001, true));
		Assertions.assertEquals(90725296, OPGAVE.solve(10000, true));
		Assertions.assertEquals(90740037, OPGAVE.solve(10001, true));
		Assertions.assertEquals(816457368, OPGAVE.solve(30000, true));
		Assertions.assertEquals(816512333, OPGAVE.solve(30001, true));
	}

	@Test
	public void testSolveInfiniteFastEmpty()
	{
		Assertions.assertEquals(1, EMPTY.solveInfiniteFast(0));
		Assertions.assertEquals(1 + 4 * 2, EMPTY.solveInfiniteFast(2));
		Assertions.assertEquals(1 + 4 * 2 * (1 + 2), EMPTY.solveInfiniteFast(4));
		Assertions.assertEquals(1 + 4 * 2 * (1 + 2 + 3), EMPTY.solveInfiniteFast(6));
		Assertions.assertEquals(1 + 4 * 2 * (1 + 2 + 3 + 4), EMPTY.solveInfiniteFast(8));
		Assertions.assertEquals(1 + 4 * 2 * (1 + 2 + 3 + 4 + 5), EMPTY.solveInfiniteFast(10));
		Assertions.assertEquals(1 + 4 * 2 * (1 + 2 + 3 + 4 + 5 + 6), EMPTY.solveInfiniteFast(12));
		Assertions.assertEquals(1 + 4 * 2 * (1 + 2 + 3 + 4 + 5 + 6 + 7), EMPTY.solveInfiniteFast(14));
		Assertions.assertEquals(1 + 4 * 2 * (1 + 2 + 3 + 4 + 5 + 6 + 7 + 8), EMPTY.solveInfiniteFast(16));
		Assertions.assertEquals((1 + 1) * (1 + 1), EMPTY.solveInfiniteFast(1));
		Assertions.assertEquals((3 + 1) * (3 + 1), EMPTY.solveInfiniteFast(3));
		Assertions.assertEquals((5 + 1) * (5 + 1), EMPTY.solveInfiniteFast(5));
		Assertions.assertEquals((7 + 1) * (7 + 1), EMPTY.solveInfiniteFast(7));
		Assertions.assertEquals((9 + 1) * (9 + 1), EMPTY.solveInfiniteFast(9));
		Assertions.assertEquals((11 + 1) * (11 + 1), EMPTY.solveInfiniteFast(11));
		Assertions.assertEquals((13 + 1) * (13 + 1), EMPTY.solveInfiniteFast(13));
		Assertions.assertEquals((15 + 1) * (15 + 1), EMPTY.solveInfiniteFast(15));
	}

	@Test
	public void testSolveInfiniteFastSimple()
	{
		Assertions.assertEquals(1, SIMPLE.solveInfiniteFast(0));
		Assertions.assertEquals(4, SIMPLE.solveInfiniteFast(1));
		Assertions.assertEquals(8, SIMPLE.solveInfiniteFast(2));
		Assertions.assertEquals(14, SIMPLE.solveInfiniteFast(3));
		Assertions.assertEquals(23, SIMPLE.solveInfiniteFast(4));
		Assertions.assertEquals(32, SIMPLE.solveInfiniteFast(5));
		Assertions.assertEquals(43, SIMPLE.solveInfiniteFast(6));
	}

	@Test
	public void testSolveInfiniteFastOpgave()
	{
		Assertions.assertEquals(1, OPGAVE.solveInfiniteFast(0));
		Assertions.assertEquals(4, OPGAVE.solveInfiniteFast(1));
		Assertions.assertEquals(909271, OPGAVE.solveInfiniteFast(1000));
		Assertions.assertEquals(911429, OPGAVE.solveInfiniteFast(1001));
		Assertions.assertEquals(22686298, OPGAVE.solveInfiniteFast(5000));
		Assertions.assertEquals(22695072, OPGAVE.solveInfiniteFast(5001));
		Assertions.assertEquals(90725296, OPGAVE.solveInfiniteFast(10000));
		Assertions.assertEquals(90740037, OPGAVE.solveInfiniteFast(10001));
		Assertions.assertEquals(816457368, OPGAVE.solveInfiniteFast(30000));
		Assertions.assertEquals(816512333, OPGAVE.solveInfiniteFast(30001));
	}
}