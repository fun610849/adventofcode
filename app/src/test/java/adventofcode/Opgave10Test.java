package adventofcode;

import org.junit.jupiter.api.Test;

public class Opgave10Test
	extends Tests
{
	public static final Opgave10 OPGAVE_10 = new Opgave10(Opgave.open("input10.txt"));

	@Test
	public void test()
	{
		System.out.println(OPGAVE_10.part1());
		System.out.println(OPGAVE_10.part2());
	}
}
