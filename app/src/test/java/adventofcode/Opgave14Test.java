package adventofcode;

import org.junit.jupiter.api.Test;

public class Opgave14Test
	extends Tests
{
	public static final Opgave14 OPGAVE_14 = new Opgave14(Opgave.open("input14.txt"));

	@Test
	public void test()
	{
		System.out.println(OPGAVE_14.part2());
	}
}