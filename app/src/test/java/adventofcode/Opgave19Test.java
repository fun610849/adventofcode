package adventofcode;

import nl.novit.parser.exception.ExceptionParserInput;
import org.junit.jupiter.api.Test;

public class Opgave19Test
	extends Tests
{
	public static final Opgave19 OPGAVE_19 = new Opgave19(Opgave.open("input19.txt"));

	@Test
	public void test()
		throws ExceptionParserInput
	{
		System.out.println(OPGAVE_19.part1());
	}
}