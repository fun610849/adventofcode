package adventofcode;

import nl.novit.parser.exception.ExceptionParserInput;
import nl.novit.util.Tuple2;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.util.SortedSet;

public class Opgave18Test
	extends Tests
{
	public static final Opgave18 OPGAVE_18 = new Opgave18(Opgave.open("input18.txt"));

	@Test
	public void test()
		throws ExceptionParserInput
	{
		Opgave18 opgave18 = new Opgave18
			(
				new ByteArrayInputStream
					(
						(
							"R 2 (#111111)\n"
								+ "D 2 (#111111)\n"
								+ "L 2 (#111111)\n"
								+ "U 2 (#111111)\n"
						).getBytes()
					)
			);
		SortedSet<Edge18> edges = opgave18.edgesPart1();
		Tuple2<Vector2DInt, Vector2DInt> bbox = Opgave18.bbox(edges);
		Assertions.assertEquals(8, edges.size());
		Assertions.assertEquals(-1, bbox.value0.x);
		Assertions.assertEquals(-1, bbox.value0.y);
		Assertions.assertEquals(5, bbox.value1.x);
		Assertions.assertEquals(5, bbox.value1.y);
		Assertions.assertTrue(Opgave18.inside(edges, new Vector2DInt(0, 0)));
		Assertions.assertTrue(Opgave18.inside(edges, new Vector2DInt(2, 0)));
		Assertions.assertTrue(Opgave18.inside(edges, new Vector2DInt(4, 0)));
		Assertions.assertTrue(Opgave18.inside(edges, new Vector2DInt(4, 2)));
		Assertions.assertTrue(Opgave18.inside(edges, new Vector2DInt(4, 4)));
		Assertions.assertFalse(Opgave18.inside(edges, new Vector2DInt(2, 2)));
		Assertions.assertEquals(8, Opgave18.inside(edges));
		SortedSet<Edge18> path = Opgave18.outside(edges);
		Assertions.assertEquals(4, path.size());
		Assertions.assertEquals(9, Opgave18.inside(path));
	}
}