package adventofcode;

import org.junit.jupiter.api.Test;

public class Opgave24Test
	extends Tests
{
	public static long randomSmall()
	{
		return (long) (RANDOM.nextDouble() * 100);
	}

	public static long randomLarge()
	{
		return (long) (RANDOM.nextDouble() * 1000000d);
	}

	public static Path path()
	{
		Vector3DFraction position =
			new Vector3DFraction
				(
					randomLarge(),
					randomLarge(),
					randomLarge()
				);
		Vector3DFraction speed =
			new Vector3DFraction
				(
					randomSmall(),
					randomSmall(),
					randomSmall()
				);
		return new Path(position, speed);
	}

	@Test
	public void testTimes()
	{
		int tests = 100000;
		for (int test = 0; test < tests; test++)
		{
			Path a = path();
			Path b = path();
			a.collision(b);
		}
	}
}