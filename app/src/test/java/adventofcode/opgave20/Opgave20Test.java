package adventofcode.opgave20;

import adventofcode.Opgave;
import adventofcode.Tests;
import nl.novit.util.Algorithm;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.*;

public class Opgave20Test
	extends Tests
{
	@Test
	public void testPart1()
	{
		Opgave20 opgave20 = new Opgave20(Opgave.open("input20.txt"));
		Assertions.assertEquals(925955316, opgave20.part1());
	}

	@Test
	public void testSolvePart1()
	{
		Opgave20 opgave20 = new Opgave20(Opgave.open("input20.txt"));
		Assertions.assertEquals(6878171954L, opgave20.solvePart1(100000000).total());
	}

	@Test
//	@Disabled
	public void testPart2()
	{
		Opgave20 opgave20 = new Opgave20(Opgave.open("input20.txt"));
		System.out.println(opgave20.part2());
//		Assertions.assertEquals(925955316, opgave20.part2());
	}

	@Test
	public void testPart2Simple()
	{
		Opgave20 opgave20 = new Opgave20(Opgave.open("simple20.txt"));
		System.out.println(opgave20.part2());
	}

	@Test
	public void experiment()
	{
		Opgave20 opgave20 = new Opgave20(Opgave.open("input20.txt"));
		int count = 0;
		for (Mod mod: opgave20.mods.values())
		{
			if (mod instanceof ModConjunction)
			{
//				count ++;
				ModConjunction conjunction = (ModConjunction) mod;
//				System.out.println(conjunction.name);
				if (conjunction.ins.size() == 1)
				{
//					count++;
				}
				for (Mod out: conjunction.outs)
				{
//					System.out.println(out);
				}
			}
			if (mod instanceof ModFlipFlop)
			{
				ModFlipFlop flipflop = (ModFlipFlop) mod;
				if (flipflop.ins.size() == 1 && flipflop.ins.get(0).outs.size() == 1)
				{
					count++;
				}
				for (Mod out: flipflop.outs)
				{
//					System.out.println(out);
				}
			}
		}
		System.out.println(count);
//		opgave20.pushOnce(new HighLow());
	}

	@Test
	public void testGcd()
	{
		long x = 3911;
		x = Algorithm.gcd(x, 4021);
		x = Algorithm.gcd(x, 3907);
		x = Algorithm.gcd(x, 3931);
		System.out.println(x);
		System.out.println(3911L * 4021 * 3907 * 3931);
	}

	@Test
	public void experiment2()
	{
		Opgave20 opgave20 = new Opgave20(Opgave.open("input20.txt"));
		opgave20.pushButton();
		long max = 0;
		HighLow old = new HighLow();
		while (max <= 335)
		{
			opgave20.pushButton();
			HighLow sum = opgave20.sum();
			HighLow subtract = sum.subtract(old);
			max = Math.max(max, subtract.total());
			old = sum;
		}
		System.out.println(max);
//		opgave20.pushOnce(new HighLow());
	}

	@Test
	public void testFindPaths()
	{
		Opgave20 opgave20 = new Opgave20(Opgave.open("input20.txt"));
		List<List<Mod>> paths = new ArrayList<>();
		paths(new TreeSet<>(), opgave20.test, opgave20.button, new LinkedList<>(), paths);
		Assertions.assertEquals(33, paths.size());
		Assertions.assertEquals(opgave20.mods.size() + 1, modsInPaths(paths).size());
	}

	public Set<Mod> modsInPaths(List<List<Mod>> paths)
	{
		Set<Mod> result = new TreeSet<>();
		paths.forEach
			(
				path -> path.forEach
					(
						mod -> result.add(mod)
					)
			);
		return result;
	}

	public void paths(Set<Mod> visited, Mod current, Mod target, LinkedList<Mod> path, List<List<Mod>> result)
	{
		if (visited.add(current))
		{
			path.addLast(current);
			if (current == target)
			{
				result.add(new ArrayList<>(path));
			}
			for (Mod next: current.ins)
			{
				paths(visited, next, target, path, result);
			}
			path.removeLast();
			visited.remove(current);
		}
	}
}